import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import functions as fs
import pickle

time_at_script_start = datetime.datetime.now()

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("gain_scan_rfko",
                                                        "gain_scan_rfko")

japc = pyjapc.PyJapc(noSet=False)
japc.setSelector("CPS.USER.MD4")

fig, ax = plt.subplots(tight_layout=True)
fig.canvas.manager.set_window_title(f'xsec monitor started at: {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')
fig.suptitle(f'xsec monitor {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')


time_list = [time_at_script_start.strftime("%Hh%Mm%Ss")]

# Data we want to save
timestamp_list = []
data_list = ["F61.XSEC023-I1/SpillData",
            "F61.XSEC023-I2/SpillData",
            "F61.XSEC023-I1/Acquisition",
            "F61.XSEC023-I2/Acquisition",
            "F61.XSEC023-I1/IntensityMeasurement",
            "F61.XSEC023-I2/IntensityMeasurement",
            "PR.MPC/REF.PPPL",
            "PR.BQL72/Setting"]
data_dict = {data: [] for data in data_list}


chirpStartFreqH = 0.32
chirpStopFreqH = 0.333
nbOfTurns = 417

gain_array = np.linspace(0.05,0.6,3)
i=0

japc.setParam("PR.BQL72/Setting#chirpStartFreqH", chirpStartFreqH)
japc.setParam("PR.BQL72/Setting#chirpStopFreqH", chirpStopFreqH)
#japc.setParam("PR.BQL72/Setting#exLength", nbOfTurns)
#print (f'set number of turns to {nbOfTurns}')
japc.setParam("PR.BQL72/Setting#exAmplitudeH", gain_array[0])



def myCallback(parameterName, newValue):

    global i
    global time_list

    #ax.clear()



    # Raw data
    intensity  = japc.getParam("F61.XSEC023-I1/IntensityMeasurement#intensity")
    gain = japc.getParam("PR.BQL72/Setting#exAmplitudeH")

    Bfield = japc.getParam('PR.MPC/REF.PPPL#REF4')
    Bfield_plateau = Bfield[0]

    time = datetime.datetime.now()
    time_list.append(time.strftime("%Hh%Mm%Ss"))
    fig.suptitle(time.strftime("RFKO gain scan on XSEC23 "+str(Bfield_plateau)+" [G] \n Most recent acquisition: %Hh%Mm%Ss"))

    # Append all new data for the new shot
    timestamp_list.append(time)
    for data in data_list:
        data_small = japc.getParam(data)
        data_dict[data].append(data_small)

    ax.scatter(gain, intensity, c='b')
    ax.set_xlabel('Gain')
    ax.set_ylabel('Intensity')

    fig.canvas.draw() # Update the plot


    try: # Set gain
        japc.setParam("PR.BQL72/Setting#exAmplitudeH", gain_array[i])
        print (f'Set gain to {gain_array[i]}')
    except:
        print('finish array')
        japc.setParam("PR.BQL72/Setting#exAmplitudeH", 0.0) #Turn off RFKO
    i += 1

    try:
        # Pickle data dump
        pickle.dump((timestamp_list, data_dict),
                    open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
    except:
        print('Error on saving data json pickle')
    try:
        # Save figure
        fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                    transparent=False,
                    bbox_inches='tight');
    except:
        print('error on saving image')


japc.subscribeParam("F61.XSEC023-I1/SpillData#semSpillData", myCallback)
japc.startSubscriptions()
fig.show()
