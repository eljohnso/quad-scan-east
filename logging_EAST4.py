import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import functions as fs
import pickle
import os

def convert_bytes(num):
    """
    this function will convert bytes to MB.... GB... etc
    """
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0

def file_size(file_path):
    """
    this function will return the file size
    """
    if os.path.isfile(file_path):
        file_info = os.stat(file_path)
        return convert_bytes(file_info.st_size)


time_at_script_start = datetime.datetime.now()

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("logging_EAST4",
                                                        "logging_EAST4")

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST4")

time_list = [time_at_script_start.strftime("%Hh%Mm%Ss")]

# Data we want to save
timestamp_list = []
data_list = [
            "PR.BQL72/Setting",
            "BXMWPC_2080/Setting#highVoltageActual",
            ]
data_dict = {data: [] for data in data_list}

def myCallback(parameterName, newValue):

    # Append all new data for the new shot
    time = datetime.datetime.now()
    timestamp_list.append(time)
    print(f"{time.strftime('%Hh%Mm%Ss')}")
    for data in data_list:
        try:
            data_small = japc.getParam(data)
            data_dict[data].append(data_small)
        except: # In case the setting we read does not require a selector
            data_small = japc.getParam(data, timingSelectorOverride="")
            data_dict[data].append(data_small)

    try:
        # Pickle data dump
        pickle.dump((timestamp_list, data_dict),
                    open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))

        # Lets check the file size
        file_path = root_folder + "/" + folder_name + "/" + folder_name + ".p"
        print(file_size(file_path))
        print("")

    except:
        print('Error on saving data through pickle')

japc.subscribeParam("F61.XSEC023-I1/SpillData#semSpillData", myCallback)
japc.startSubscriptions()
