import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import matplotlib.dates as mdates
import datetime
import os
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
import sys
import time

print(f"Steering helper")

# We start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=True)
user = "CPS.USER.EAST3"
japc.setSelector(user)

btv_name_list = ["F61.BTV012",
		"F62.BTV002",
		"T8.BTV020",
		"T8.BTV035",
		"T8.BTV096"]
btv_name_list = btv_name_list[::]

btv_dict = {btv_name: [] for btv_name in btv_name_list}



# Initial BTV parameters
print(80*"_")
print("Initial BTV settings:\n")


for btv_name in btv_name_list:
	print(btv_name)
	japc = pyjapc.PyJapc(noSet=False)
	japc.setSelector(user)
	print("Selecting all images to acquire")
	japc.setParam(btv_name+"/MultiplexedSetting#reqUserImageSelection", [True,True,True,True,True,True,True])


	japc.setSelector("") #Parameters that requires no selector
	print("Turning on Camera Switch")
	japc.setParam(btv_name+"/Setting#screenSelect", 'FIRST')

	print("Inserting Screen")
	japc.setParam(btv_name+"/Setting#cameraSwitch", 'CCD_ON_RAD_STDBY')

	print("Turning On Light")
	japc.setParam(btv_name+"/Setting#lampSwitch", 'ON')
	print(" ")

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector(user)
print(80*"_"+"\n")

##################
# Animation part #
##################

def myCallback(parameterName, newValue, header):
	print(f"New value for {parameterName}")

	for btv_name in btv_name_list:
		btv_data_image = japc.getParam(btv_name+"/Image")
		btv_dict[btv_name].append(btv_data_image)

japc.subscribeParam("F61.BTV012/Image", myCallback, getHeader=True)
japc.startSubscriptions()

fig, ax = plt.subplots(1,len(btv_name_list),tight_layout=True, figsize=(12,2))
fig.canvas.manager.set_window_title('Steering helper')
fig.show()

def animate_func(i):
	j=0
	for btv_name in btv_dict:
		ax[j].clear()
		try:
			pixel_y = len(btv_dict[btv_name][-1]["imagePositionSet2"][0])
			try:
				
				image = btv_dict[btv_name][-1]["imageSet"][i]
				reshaped_image = image.reshape(pixel_y,-1)
				
			except:
				image = btv_dict[btv_name][-1]["imageSet"][0] # If there are not enough frame, just send first frame
				reshaped_image = image.reshape(pixel_y,-1)
			start = time.process_time()
			ax[j].imshow(reshaped_image, cmap="plasma")
			#print(time.process_time() - start)
			ax[j].set_title(f"{btv_name } Acq: {i}")
			j+=1
					
		except:
			print ("skipping empty data")
			pass
	return reshaped_image

anim = animation.FuncAnimation(
			       fig, 
			       animate_func,
			       frames = 7,
			       interval = 100, # in ms
			       )











