import jpype
jpype.startJVM(jpype.getDefaultJVMPath())
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from skimage import filters
from madxtools.math import do_gaussian_fit, gaussian_function, pb_ion_rigidity
from madxtools import transfer_function
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import os
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

# Function to update the Combobox with .pickle files found in a directory
def update_combobox():
    global selected_directory
    selected_directory = filedialog.askdirectory()  # Ask user to select a directory
    if selected_directory:
        pickle_files = find_pickle_files(selected_directory)
        file_combobox['values'] = pickle_files
        file_combobox.configure(height=len(pickle_files) + 1)  # Set the visible items

# Function to recursively find .pickle files in a directory and its subdirectories
def find_pickle_files(directory):
    pickle_files = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.p'):
                # Extract only the filename without the path
                filename = os.path.basename(file)
                pickle_files.append(filename)
                pickle_files = sorted(pickle_files)
    return pickle_files

# Function to populate the Combobox with folders and files in a directory
def populate_combobox():
    global selected_directory
    selected_directory = filedialog.askdirectory(initialdir='../')  # Ask user to select a directory
    if selected_directory:
        print("Finding files")
        pickle_files = find_pickle_files(selected_directory)
        file_combobox['values'] = pickle_files
        file_combobox.configure(height=len(pickle_files) + 1, width=60)  # Set the visible items
        print("Done !")

# Function to update data based on the selected file
def update_data():
    global data
    selected_file = file_combobox.get()  # Get the selected filename from the Combobox
    if selected_file:
        selected_file_path = os.path.join(selected_directory, selected_file[:-2] + "/", selected_file)
        if os.path.exists(selected_file_path):  # Check if the file exists
            with open(selected_file_path, 'rb') as f:
                data = pickle.load(f)
            # Add your data processing code here
            # For example, you can print the loaded data
            print(f"Loaded data from {selected_file_path}")

            E_cin_per_nucleon = float(E_cin_entry.get())
            print(f"E_cin_per_nucleon = {E_cin_per_nucleon}")

            timestamp_list = data[0]
            btv_dict = data[1]
            btv_image_dict = data[2]
            converter_dict = data[3]
            logical_k_dict = data[4]
            intensity_list = data[5]

            for index_to_remove in reversed([0,]): # Remove the first index and reversed to prevent index shifting
                timestamp_list.pop(index_to_remove)
                for key in btv_dict:
                    btv_dict[key].pop(index_to_remove)
                for key in btv_image_dict:
                    btv_image_dict[key].pop(index_to_remove)
                for key in converter_dict:
                    converter_dict[key].pop(index_to_remove)
                for key in logical_k_dict:
                    logical_k_dict[key].pop(index_to_remove)
                intensity_list.pop(index_to_remove)

            acq = 2
            remove_background = True
            saturation_threshold = 1000
            error_lim = 0.6

            print("Calculating beam size")

            sigH = []
            sigV = []
            for shot_number in range(len(btv_dict['F61D.BTV010/Acquisition'])):

                    # Convert to an pixel array
                    pixel_y = len(btv_image_dict['F61D.BTV010/Image'][shot_number]["imagePositionSet2"][acq])
                    image = btv_image_dict['F61D.BTV010/Image'][shot_number]["imageSet"][acq]
                    saturation = np.count_nonzero(image == 4095)
                    reshaped_image = image.reshape(pixel_y,-1)
                    reshaped_image = reshaped_image/4095 #Normalize
                    med_filter_image = filters.median(reshaped_image, np.ones((6, 6)))
                    med_filter_image = filters.gaussian(med_filter_image, 6)

                    if remove_background == True:
                        pixel_y = len(btv_image_dict['F61D.BTV010/Image'][shot_number]["imagePositionSet2"][0])
                        image_noise = btv_image_dict['F61D.BTV010/Image'][shot_number]["imageSet"][0]
                        reshaped_image_noise = image_noise.reshape(pixel_y,-1)
                        reshaped_image_noise = reshaped_image_noise/4095 #Normalize
                        med_filter_image_noise = filters.median(reshaped_image_noise, np.ones((6, 6)))
                        med_filter_image_noise = filters.gaussian(med_filter_image_noise, 6)
                        med_filter_image = med_filter_image - med_filter_image_noise

                    try:
                        # Horizontal
                        Hy = med_filter_image.sum(axis=0)
                        Hx = np.linspace(min(btv_dict['F61D.BTV010/Acquisition'][shot_number]["projPositionSet1"][acq]), max(btv_dict['F61D.BTV010/Acquisition'][shot_number]["projPositionSet1"][acq]), len(med_filter_image.sum(axis=0))) # Pixel calibration
                        poptH, pcovH = do_gaussian_fit(Hx, Hy)
                        # Error
                        perrH = np.sqrt(np.diag(pcovH))
                        if ((perrH[3] < error_lim) and (saturation < saturation_threshold)):
                            sigH.append(np.abs(poptH[3]))
                        else:
                            sigH.append(np.nan)
                    except Exception as e:
                        print(f"An error occurred: {e}")
                        print("Skipping error")
                        sigH.append(np.nan)


                    try:
                        # Vertical
                        Vy = med_filter_image.sum(axis=1)
                        Vx = np.linspace(min(btv_dict['F61D.BTV010/Acquisition'][shot_number]["projPositionSet2"][acq]), max(btv_dict['F61D.BTV010/Acquisition'][shot_number]["projPositionSet2"][acq]), len(med_filter_image.sum(axis=1))) # Pixel calibration
                        poptV, pcovV = do_gaussian_fit(Vx, Vy)
                        # Error
                        perrV = np.sqrt(np.diag(pcovV))
                        if ((perrV[3] < error_lim) and (saturation < saturation_threshold)):
                            sigV.append(np.abs(poptV[3]))
                        else:
                            sigV.append(np.nan)
                    except Exception as e:
                        print(f"An error occurred: {e}")
                        print("Skipping error")
                        sigV.append(np.nan)



            d = {'current_qfn01': converter_dict["F61.QFN01/MEAS.PULSE#VALUE"],
                'current_qdn02': converter_dict["F61.QDN02/MEAS.PULSE#VALUE"],
                'current_qfn03': converter_dict["F61.QFN03/MEAS.PULSE#VALUE"],
                'k_qfn01': transfer_function.k1(converter_dict["F61.QFN01/MEAS.PULSE#VALUE"], 'Q74L', pb_ion_rigidity(E_cin_per_nucleon)),
                'k_qdn02': -transfer_function.k1(converter_dict["F61.QDN02/MEAS.PULSE#VALUE"], 'Q120C', pb_ion_rigidity(E_cin_per_nucleon))*82/54,
                'k_qfn03': transfer_function.k1(converter_dict["F61.QFN03/MEAS.PULSE#VALUE"], 'QFL', pb_ion_rigidity(E_cin_per_nucleon))*82/54,
                'sigH': sigH,
                'sigV': sigV,
                }
            df = pd.DataFrame(data=d)
            df.dropna(inplace=True)

            with open(selected_directory+"/df/"+selected_file[:-2]+"_df.pickle", 'wb') as handle:
                pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)

            ax.scatter(df.k_qfn01, df.sigH)
            ax.scatter(df.k_qfn01, df.sigV)

            canvas.draw()
            print("Done !")

        else:
            print(f"File not found: {selected_file_path}")

# Create a Tkinter window
root = tk.Tk()
root.title("File Selector")

# Create a button to trigger the directory selection
select_directory_button = ttk.Button(root, text="Select Directory", command=populate_combobox)
select_directory_button.pack()

# Create a Combobox (dropdown list) to select files
file_combobox = ttk.Combobox(root, values=[], height=5, width=60)
file_combobox.pack()

# Create a label and an Entry box for E_cin_per_nucleon
E_cin_label = ttk.Label(root, text="E_cin_per_nucleon:")
E_cin_label.pack()
E_cin_entry = ttk.Entry(root)
E_cin_entry.pack()

# Create a button to trigger data update and plot
update_button = ttk.Button(root, text="Update Data and Plot", command=update_data)
update_button.pack()

# Create a Matplotlib Figure and Axes
fig, ax = plt.subplots()

# Create a Tkinter Canvas to embed the Matplotlib figure
canvas = FigureCanvasTkAgg(fig, master=root)
canvas.get_tk_widget().pack()

# Start the Tkinter main loop
root.mainloop()
