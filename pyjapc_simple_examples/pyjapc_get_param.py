import numpy as np
import pyjapc

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")
current = japc.getParam("F61.QFN01/MEAS.PULSE#VALUE")
print (f"QFNO1 measured current = {current} [A]")

