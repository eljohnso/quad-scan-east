import numpy as np
import pyjapc

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")
current = 600
japc.setParam("F61.QFN01/REF.PULSE.AMPLITUDE#VALUE",current)
print(f"Set the QFN01 current to {current}")
