import numpy as np
import pyjapc

japc = pyjapc.PyJapc()
japc.setSelector("CPS.USER.EAST3")

def myCallback(parameterName, newValue):
	print(f"New value for {parameterName} is: {newValue}")

japc.subscribeParam("F61.QFN01/MEAS.PULSE#VALUE", myCallback)
japc.startSubscriptions()
