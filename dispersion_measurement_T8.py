import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.dates as mdates
from matplotlib import cm
import datetime
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
import skimage
from skimage import filters
import time
import itertools
from madxtools.transfer_function import *
from madxtools.math import *

time_at_script_start = datetime.datetime.now()

print(f'''
Dispersion measurement
{time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}                                                                                                                                                                                     
''')

# Start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=True)
japc.rbacLogin()
user = "CPS.USER.MD3"
japc.setSelector(user)

# BCT in the PSB to skip low intenstiy shots
bct = "EE.BCT10/Acquisition#totalIntensitySingle"
bct_threshold = 1
PSB_user = "LEI.USER.MDNOM"

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("dispersion_measurement_data",
                                                        "dispersion_measurement_data")

# Data we want to save
timestamp_list = []
intensity_list = []
bfield_measured_list = []

btv_name_list = ["F62.BTV002"]
btv_dict = {btv_name + "/Acquisition": [] for btv_name in btv_name_list}
btv_image_dict = {btv_name + "/Image": [] for btv_name in btv_name_list}

filter_wheel = {"no_filter": 'ZERO',
                "filter_1": 'FIRST',
                "filter_2": 'SECOND',
                "filter_3": 'THIRD'}
filter_selection = "no_filter"
print(f"Selected filter: {filter_selection}\n")

converter_name_list = ["F61.QFN01/MEAS.PULSE#VALUE", # Quadrupoles
                       "F61.QDN02/MEAS.PULSE#VALUE",
                       "F61.QFN03/MEAS.PULSE#VALUE",
                       "F61.QDN04/MEAS.PULSE#VALUE",
                       "T8.QFN05/MEAS.PULSE#VALUE",
                       "T8.QDN06/MEAS.PULSE#VALUE",
                       "T8.QDN07/MEAS.PULSE#VALUE",
                       "T8.QFN08/MEAS.PULSE#VALUE",
                       "F61.DHZ01/MEAS.PULSE#VALUE", # Correctors
                       "F61.DVT01/MEAS.PULSE#VALUE",
                       "T8.DHZ02/MEAS.PULSE#VALUE",
                       "T8.DVT02/MEAS.PULSE#VALUE",
                       "T8.DHZ03/MEAS.PULSE#VALUE",
                       "T8.DVT03/MEAS.PULSE#VALUE",
                       "F61.BHZ01.A/MEAS.PULSE#VALUE", # Dipoles
                       "F61.BHZ01.B/MEAS.PULSE#VALUE",
                       "F61.BHZ02.A/MEAS.PULSE#VALUE",
                       "F61.BHZ02.B/MEAS.PULSE#VALUE",
                       "T8.BHZ03/MEAS.PULSE#VALUE",
                       "T8.BHZ04/MEAS.PULSE#VALUE",
                       "T8.BHZ05/MEAS.PULSE#VALUE"
                       ]

converter_dict = {converter_name: [] for converter_name in converter_name_list}

logical_k_list = ["logical.F61.QFN01/K_FUNC_LIST#value",
                  "logical.F61.QDN02/K_FUNC_LIST#value",
                  "logical.F61.QFN03/K_FUNC_LIST#value",
                  "logical.F61.QDN04/K_FUNC_LIST#value",
                  "logical.T8.QFN05/K_FUNC_LIST#value",
                  "logical.T8.QDN06/K_FUNC_LIST#value",
                  "logical.T8.QDN07/K_FUNC_LIST#value",
                  "logical.T8.QFN08/K_FUNC_LIST#value",
                  "logical.F61.DHZ01/KL_FUNC_LIST#value",  # Correctors
                  "logical.F61.DVT01/KL_FUNC_LIST#value",
                  "logical.T8.DHZ02/KL_FUNC_LIST#value",
                  "logical.T8.DVT02/KL_FUNC_LIST#value",
                  "logical.T8.DHZ03/KL_FUNC_LIST#value",
                  "logical.T8.DVT03/KL_FUNC_LIST#value",
                  "logical.F61.BHZ01/KL_FUNC_LIST#value",  # Dipoles
                  "logical.F61.BHZ02/KL_FUNC_LIST#value",
                  "logical.T8.BHZ03/KL_FUNC_LIST#value",
                  "logical.T8.BHZ04/KL_FUNC_LIST#value",
                  "logical.T8.BHZ05/KL_FUNC_LIST#value"
                  ]
logical_k_dict = {logical_k: [] for logical_k in logical_k_list}


bfield_list = ['Bfield_set']
bfield_dict = {bfield: [] for bfield in bfield_list}

sigma_dict = {"sig_h0": [], "sig_h1": [], "sig_h2": [], "sig_h3": [], "sig_h4": [], "sig_h5": [], "sig_h6": [],
              "sig_v0": [], "sig_v1": [], "sig_v2": [], "sig_v3": [], "sig_v4": [], "sig_v5": [], "sig_v6": []}
sigma_err_dict = {"sig_h0": [], "sig_h1": [], "sig_h2": [], "sig_h3": [], "sig_h4": [], "sig_h5": [], "sig_h6": [],
                  "sig_v0": [], "sig_v1": [], "sig_v2": [], "sig_v3": [], "sig_v4": [], "sig_v5": [], "sig_v6": []}
mu_dict = {"mu_h0": [], "mu_h1": [], "mu_h2": [], "mu_h3": [], "mu_h4": [], "mu_h5": [], "mu_h6": [],
              "mu_v0": [], "mu_v1": [], "mu_v2": [], "mu_v3": [], "mu_v4": [], "mu_v5": [], "mu_v6": []}


bpm_list = ['PS-LOG-BPM-IRRAD-UCAP_BPM_01/Positions',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_02/Positions',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_03/Positions',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_04/Positions', ]
bpm_dict = {bpm: [] for bpm in bpm_list}

bpm_profiles_list = ['PS-LOG-BPM-IRRAD-UCAP_BPM_01/ProfilesAcquisition',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_02/ProfilesAcquisition',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_03/ProfilesAcquisition',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_04/ProfilesAcquisition', ]
bpm_profiles_dict = {bpm_profiles: [] for bpm_profiles in bpm_profiles_list}

mwpc_list = ['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainProfilesAcquisition',
            'PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions',
            'PS-LOG-MWPC-UCAP_IRRAD_2080/HighGainProfilesAcquisition',
            'PS-LOG-MWPC-UCAP_IRRAD_2080/HighGainPositions', ]
mwpc_dict = {mwpc: [] for mwpc in mwpc_list}


# Initial BTV parameters
print(80 * "_")
print("Initial BTV settings:\n")

for btv_name in btv_name_list:
    print(btv_name)
    japc = pyjapc.PyJapc(noSet=False)
    japc.setSelector(user)
    print("Image Selection: True,True,True,True,True,True,True")
    japc.setParam(btv_name + "/MultiplexedSetting#reqUserImageSelection", [True, True, True, True, True, True, True])

    japc.setSelector("")  # Parameters that requires no selector
    print("SCREEN: In")
    japc.setParam(btv_name + "/Setting#screenSelect", 'FIRST')  # Screen Out = "ZERO", In = "FIRST"

    print("Camera Switch: ON")
    japc.setParam(btv_name + "/Setting#cameraSwitch", 'CCD_ON_RAD_STDBY')

    print("Lamp Switch: OFF")
    japc.setParam(btv_name + "/Setting#lampSwitch", 'OFF')

    print(f"Filter: {filter_selection}")
    japc.setParam(btv_name + "/Setting#filterSelect", filter_wheel[filter_selection])
    print(" ")

japc.setSelector(user)
print(80 * "_" + "\n")

print("Starting SCAN\n")
iteration_number = 0

# Quadrupoles
qfn01_i = japc.getParam('logical.F61.QFN01/I_FUNC_LIST#value')
qdn02_i = japc.getParam('logical.F61.QDN02/I_FUNC_LIST#value')
qfn03_i = japc.getParam('logical.F61.QFN03/I_FUNC_LIST#value')
qdn04_i = japc.getParam('logical.F61.QDN04/I_FUNC_LIST#value')
qfn05_i = japc.getParam('logical.T8.QFN05/I_FUNC_LIST#value')
qdn06_i = japc.getParam('logical.T8.QDN06/I_FUNC_LIST#value')
qdn07_i = japc.getParam('logical.T8.QDN07/I_FUNC_LIST#value')
qfn08_i = japc.getParam('logical.T8.QFN08/I_FUNC_LIST#value')

# Correctors
dhz01_i = japc.getParam('logical.F61.DHZ01/I_FUNC_LIST#value')
dvt01_i = japc.getParam('logical.F61.DVT01/I_FUNC_LIST#value')
dhz02_i = japc.getParam('logical.T8.DHZ02/I_FUNC_LIST#value')
dvt02_i = japc.getParam('logical.T8.DVT02/I_FUNC_LIST#value')
dhz03_i = japc.getParam('logical.T8.DHZ03/I_FUNC_LIST#value')
dvt03_i = japc.getParam('logical.T8.DVT03/I_FUNC_LIST#value')

# Dipoles
bhz01_i = japc.getParam('logical.F61.BHZ01/I_FUNC_LIST#value')
bhz02_i = japc.getParam('logical.F61.BHZ02/I_FUNC_LIST#value')
bhz03_i = japc.getParam('logical.T8.BHZ03/I_FUNC_LIST#value')
bhz04_i = japc.getParam('logical.T8.BHZ04/I_FUNC_LIST#value')
bhz05_i = japc.getParam('logical.T8.BHZ05/I_FUNC_LIST#value')


# Define the I for the magnets
number_of_ekin_to_scan = 8
repetition_number = 6
length_function_ms = 540 # ms

# Quadrupoles
current1 = japc.getParam('logical.F61.QFN01/I_FUNC_LIST#value')[0][1][0]
current2 = japc.getParam('logical.F61.QDN02/I_FUNC_LIST#value')[0][1][0]
current3 = japc.getParam('logical.F61.QFN03/I_FUNC_LIST#value')[0][1][0]
current4 = japc.getParam('logical.F61.QDN04/I_FUNC_LIST#value')[0][1][0]
current5 = japc.getParam('logical.T8.QFN05/I_FUNC_LIST#value')[0][1][0]
current6 = japc.getParam('logical.T8.QDN06/I_FUNC_LIST#value')[0][1][0]
current7 = japc.getParam('logical.T8.QDN07/I_FUNC_LIST#value')[0][1][0]
current8 = japc.getParam('logical.T8.QFN08/I_FUNC_LIST#value')[0][1][0]

# Correctors
dhz01_i_value = japc.getParam('logical.F61.DHZ01/I_FUNC_LIST#value')[0][1][0]
dvt01_i_value = japc.getParam('logical.F61.DVT01/I_FUNC_LIST#value')[0][1][0]
dhz02_i_value = japc.getParam('logical.T8.DHZ02/I_FUNC_LIST#value')[0][1][0]
dvt02_i_value = japc.getParam('logical.T8.DVT02/I_FUNC_LIST#value')[0][1][0]
dhz03_i_value = japc.getParam('logical.T8.DHZ03/I_FUNC_LIST#value')[0][1][0]
dvt03_i_value = japc.getParam('logical.T8.DVT03/I_FUNC_LIST#value')[0][1][0]

# Dipoles
bhz01_i_value = japc.getParam('logical.F61.BHZ01/I_FUNC_LIST#value')[0][1][0]
bhz02_i_value = japc.getParam('logical.F61.BHZ02/I_FUNC_LIST#value')[0][1][0]
bhz03_i_value = japc.getParam('logical.T8.BHZ03/I_FUNC_LIST#value')[0][1][0]
bhz04_i_value = japc.getParam('logical.T8.BHZ04/I_FUNC_LIST#value')[0][1][0]
bhz05_i_value = japc.getParam('logical.T8.BHZ05/I_FUNC_LIST#value')[0][1][0]

plateau_number = 0 # Plateau where the flattop B-field is set

min_input_ekin = 0.65
max_input_ekin = 2.7

def validate_float(value):
    try:
        float_value = float(value)
        return min_input_ekin <= float_value <= max_input_ekin
    except ValueError:
        return False

def set_bfield_scan(E_cin_per_nucleon):
    if validate_float(E_cin_per_nucleon):
        new_bfield = pb_ion_bfield(E_cin_per_nucleon)
        print(f"Setting to {round(new_bfield,1)} [G]")

        bfield = japc.getParam("PR.MPC/REF.PPPL")
        bfield["REF4"][plateau_number] = new_bfield
        try:
            japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
        except Exception as e:
            print('Ignored pyjapc error')
            print(e)
    else:
        print("Error: Ekin out of range or non-float")

print('Setting the Initial settings:')

# Set the B-field of the beam
energy = np.linspace(1.98, 2.02, number_of_ekin_to_scan) #2 GeV/u init is 5096 G
energy = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in energy))
print(f"Setting B-field to: {round(energy[iteration_number], 3)}")
bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")
print(f'Actual B-field at flattop = {bfield[0]} [G]')
new_energy = energy[iteration_number]

set_bfield_scan(new_energy)

# japc.rbacLogin()

#Quadrupoles
qfn01_i[0] = [[0, length_function_ms], [current1, current1]]
qdn02_i[0] = [[0, length_function_ms], [current2, current2]]
qfn03_i[0] = [[0, length_function_ms], [current3, current3]]
qdn04_i[0] = [[0, length_function_ms], [current4, current4]]
qfn05_i[0] = [[0, length_function_ms], [current5, current5]]
qdn06_i[0] = [[0, length_function_ms], [current6, current6]]
qdn07_i[0] = [[0, length_function_ms], [current7, current7]]
qfn08_i[0] = [[0, length_function_ms], [current8, current8]]
japc.setParam('logical.F61.QFN01/I_FUNC_LIST#value', qfn01_i)
japc.setParam('logical.F61.QDN02/I_FUNC_LIST#value', qdn02_i)
japc.setParam('logical.F61.QFN03/I_FUNC_LIST#value', qfn03_i)
japc.setParam('logical.F61.QDN04/I_FUNC_LIST#value', qdn04_i)
japc.setParam('logical.T8.QFN05/I_FUNC_LIST#value', qfn05_i)
japc.setParam('logical.T8.QDN06/I_FUNC_LIST#value', qdn06_i)
japc.setParam('logical.T8.QDN07/I_FUNC_LIST#value', qdn07_i)
japc.setParam('logical.T8.QFN08/I_FUNC_LIST#value', qfn08_i)

# Correctors
dhz01_i[0] = [[0, length_function_ms], [dhz01_i_value, dhz01_i_value]]
dvt01_i[0] = [[0, length_function_ms], [dvt01_i_value, dvt01_i_value]]
dhz02_i[0] = [[0, length_function_ms], [dhz02_i_value, dhz02_i_value]]
dvt02_i[0] = [[0, length_function_ms], [dvt02_i_value, dvt02_i_value]]
dhz03_i[0] = [[0, length_function_ms], [dhz03_i_value, dhz03_i_value]]
dvt03_i[0] = [[0, length_function_ms], [dvt03_i_value, dvt03_i_value]]
japc.setParam('logical.F61.DHZ01/I_FUNC_LIST#value', dhz01_i)
japc.setParam('logical.F61.DVT01/I_FUNC_LIST#value', dvt01_i)
japc.setParam('logical.T8.DHZ02/I_FUNC_LIST#value', dhz02_i)
japc.setParam('logical.T8.DVT02/I_FUNC_LIST#value', dvt02_i)
japc.setParam('logical.T8.DHZ03/I_FUNC_LIST#value', dhz03_i)
japc.setParam('logical.T8.DVT03/I_FUNC_LIST#value', dvt03_i)

# Dipoles
bhz01_i[0] = [[0, length_function_ms], [bhz01_i_value, bhz01_i_value]]
bhz02_i[0] = [[0, length_function_ms], [bhz02_i_value, bhz02_i_value]]
bhz03_i[0] = [[0, length_function_ms], [bhz03_i_value, bhz03_i_value]]
bhz04_i[0] = [[0, length_function_ms], [bhz04_i_value, bhz04_i_value]]
bhz05_i[0] = [[0, length_function_ms], [bhz05_i_value, bhz05_i_value]]
japc.setParam('logical.F61.BHZ01/I_FUNC_LIST#value', bhz01_i)
japc.setParam('logical.F61.BHZ02/I_FUNC_LIST#value', bhz02_i)
japc.setParam('logical.T8.BHZ03/I_FUNC_LIST#value', bhz03_i)
japc.setParam('logical.T8.BHZ04/I_FUNC_LIST#value', bhz04_i)
japc.setParam('logical.T8.BHZ05/I_FUNC_LIST#value', bhz05_i)

print("Waiting for filter wheel to move")
time.sleep(5)
print("Done")

fig, ax = plt.subplots(2, 2, figsize=(10, 8), tight_layout=True)

ax_V_sigma = ax[1, 1].twinx()
fig.canvas.manager.set_window_title(
    f'Dispersion measurement started at: {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
fig.suptitle(f'Dispersion measurement {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
converter_dict_animation = {converter_name: [] for converter_name in converter_name_list}

##################
# Callback function, gets called everytime the value we're subscribed to gets updated
##################
ekin_old = None
def myCallback(parameterName, newValue, header):
    # Check if BCT in the PSB is above threshold
    bct_intensity = japc.getParam(bct, timingSelectorOverride=PSB_user)
    if bct_intensity > bct_threshold:
        ax[0, 0].clear()
        ax[0, 1].clear()
        ax[1, 0].clear()
        time_now = datetime.datetime.now()
        fig.suptitle(time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss"))
        print(f"New Shot for {parameterName} at {time_now.hour}h{time_now.minute}:{time_now.second}")
        global iteration_number
        global ekin_old
        print(f"Iteration number: {iteration_number}/{number_of_ekin_to_scan * repetition_number}")
        # Append timestamp
        timestamp_list.append(time_now)

        # Append intensity
        intensity = japc.getParam("PR.BCT/HotspotIntensity#dcBefEje2", timingSelectorOverride=user)
        intensity_list.append(intensity)

        # Append Bfield
        bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")
        print(f'Actual B-field at flattop = {bfield[0]} [G]')
        bfield_measured_list.append(bfield[0])

        # Append all new currents for the new shot
        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict[converter_name].append(current)

        # Append all new logical k for the new shot
        for logical_k_name in logical_k_list:
            logical_k = japc.getParam(logical_k_name)
            logical_k_dict[logical_k_name].append(logical_k)

        # Append all new energy for the new shot
        for bfield_name in bfield_list:
            bfield_dict[bfield_name].append(energy[iteration_number])

        # Append all new bpm data
        bpm_number = 0
        for bpm_name in bpm_list:
            try:
                bpm_data = japc.getParam(bpm_name)
                bpm_dict[bpm_name].append(bpm_data)
                x = []
                H = []
                V = []
                for i in range(len(bpm_dict[bpm_name])):
                    x.append(bfield_measured_list[-1])
                    H.append(bpm_dict[bpm_name][i]['HCenter'])
                    V.append(bpm_dict[bpm_name][i]['VCenter'])
                ax[1, 0].plot(x, H, marker="x",
                                       label=f"HCenter IRRAD BPM {bpm_number + 1}")
                ax[1, 0].plot(x, V, marker="x",
                                       label=f"VCenter IRRAD BPM {bpm_number + 1}")
                ax[1, 0].legend()
                ax[1, 0].set_title("IRRAD BPMs")
                bpm_number += 1
            except:
                print("failed to plot bpm")
                pass

        # Append all new bpm profiles data
        bpm_number = 0
        for bpm_profiles_name in bpm_profiles_list:
            try:
                bpm_profiles_data = japc.getParam(bpm_profiles_name)
                bpm_profiles_dict[bpm_profiles_name].append(bpm_profiles_data)
            except:
                print("failed to plot bpm profiles")
                pass


        # Append all new mwpc for the new shot
        try:
            for mwpc_name in mwpc_list:
                mwpc = japc.getParam(mwpc_name)
                mwpc_dict[mwpc_name].append(mwpc)
            x = []
            H = []
            V = []
            for i in range(len(mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions'])):
                x.append(bfield_measured_list[-1])
                H.append(mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions'][i]['HCenter'])
                V.append(mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions'][i]['VCenter'])
            ax[0, 1].scatter(x, H, marker="x", color='b', label="HCenter MWPC")
            ax[0, 1].scatter(x, V, marker="x", color='r', label="VCenter MWPC")
            ax[0, 1].set_title("MWPC")
        except:
            print('failed to get mwpc current')
            pass

        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict_animation[converter_name].append(current)
            ax[0, 0].plot(timestamp_list, converter_dict_animation[converter_name], marker="o", label=converter_name)
            ax[0, 0].set_xlabel("Local Time")
            ax[0, 0].set_ylabel("Current [A]")
            ax[0, 0].set_title("Transfer line converters")
            ax[0, 0].legend(fontsize=6)
            ax[0, 0].fmt_xdata = mdates.DateFormatter('%Y-%m-%d')  # Prettier date time
            ax[0, 0].tick_params('x', labelrotation=45)

        # Append all new btv data (projections, etc) for the new shot
        for btv_name in btv_name_list:
            btv_data = japc.getParam(btv_name + "/Acquisition")
            btv_dict[btv_name + "/Acquisition"].append(btv_data)

            try:
                H_mu_list = []
                H_mu_err_list = []
                V_mu_list = []
                V_mu_err_list = []
                H_sigma_list = []
                H_sigma_err_list = []
                V_sigma_list = []
                V_sigma_err_list = []

                colorB = iter(cm.Blues(np.linspace(1, 0, 8)))
                colorR = iter(cm.Reds(np.linspace(1, 0, 8)))
                for i in range(len(btv_dict[btv_name + "/Acquisition"][-1]["positionSet1"])):
                    cB = next(colorB)
                    cR = next(colorR)
                    # Horizontal Gaussian Fit
                    Hx = btv_dict[btv_name + "/Acquisition"][-1]["projPositionSet1"][i]
                    Hy = btv_dict[btv_name + "/Acquisition"][-1]["projDataSet1"][i]

                    try:
                        popt, pcov = fs.do_gaussian_fit(Hx, Hy)
                        H_mu_list.append(popt[2])
                        H_mu_err_list.append(pcov[2, 2] ** 0.5)
                        sigmaH = abs(popt[3])
                        muH = abs(popt[2])
                        H_sigma_list.append(sigmaH)
                        sigma_err = pcov[3, 3] ** 0.5
                        H_sigma_err_list.append(sigma_err)

                        sigma_dict["sig_h" + str(i)].append(sigmaH)
                        sigma_err_dict["sig_h" + str(i)].append(sigma_err)
                        mu_dict["mu_h" + str(i)].append(muH)

                    except:
                        # print ("failed to do the gaussian fit")
                        sigma_dict["sig_h" + str(i)].append(np.nan)
                        sigma_err_dict["sig_h" + str(i)].append(np.nan)
                        mu_dict["mu_h" + str(i)].append(np.nan)
                        pass
                    # Vertical Gaussian Fit
                    Vx = btv_dict[btv_name + "/Acquisition"][-1]["projPositionSet2"][i]
                    Vy = btv_dict[btv_name + "/Acquisition"][-1]["projDataSet2"][i]

                    try:
                        popt, pcov = fs.do_gaussian_fit(Vx, Vy)
                        V_mu_list.append(popt[2])
                        V_mu_err_list.append(pcov[2, 2] ** 0.5)
                        sigmaV = abs(popt[3])
                        muH = abs(popt[2])
                        V_sigma_list.append(sigmaV)
                        sigma_err = pcov[3, 3] ** 0.5
                        V_sigma_err_list.append(sigma_err)

                        sigma_dict["sig_v" + str(i)].append(sigmaV)
                        sigma_err_dict["sig_v" + str(i)].append(sigma_err)
                        mu_dict["mu_v" + str(i)].append(muV)


                    except:
                        # print ("failed to do the gaussian fit")
                        sigma_dict["sig_v" + str(i)].append(np.nan)
                        sigma_err_dict["sig_v" + str(i)].append(np.nan)
                        mu_dict["mu_v" + str(i)].append(np.nan)
                        pass

                    try:
                        if i==3: # This selects which acquisition to highlight
                            s=100
                            alpha=0.7
                        else:
                            s=25
                            alpha=0.5
                        ax[1, 1].scatter(bfield_measured_list[-1], muH, s=s, alpha=alpha, color=cB)
                        ax_V_sigma.scatter(bfield_measured_list[-1], muH, s=s, alpha=alpha, color=cR)
                    except:
                        print('failed plot')
                        pass

                ax[1, 1].set_title(f"Beam center on {btv_name_list[0]}")
                ax[1, 1].set_xlabel("Ekin (energy) [GeV/u]")
                ax[1, 1].set_ylabel("H beam center[mm]", color="b")
                ax[1, 1].tick_params(axis='y', labelcolor="b")
                ax_V_sigma.set_ylabel("V beam center [mm]", color="r")
                ax_V_sigma.tick_params(axis='y', labelcolor="r")
            except:
                print('failed to do the plots')

        # Append btv image to the new shot
        global btv_data_image
        global number_of_acqu
        btv_data_image = japc.getParam(btv_name_list[0] + "/Image", timingSelectorOverride=user)
        number_of_acqu = len(btv_data_image["imageSet"])

        btv_image_dict[btv_name + "/Image"].append(btv_data_image)


        try:
            # JSON data dump
            with open(root_folder + "/" + folder_name + "/" + folder_name + ".json", 'w') as outfile:
                outfile.write(json.dumps((timestamp_list, btv_dict, btv_image_dict, converter_dict, logical_k_dict,
                                          bpm_dict, bpm_profiles_dict, intensity_list, bfield_dict, bfield_measured_list, mwpc_dict, mu_dict), cls=myJSONEncoder))

            # Pickle data dump
            pickle.dump((timestamp_list, btv_dict, btv_image_dict, converter_dict, logical_k_dict,
                         bpm_dict, bpm_profiles_dict, intensity_list, bfield_dict, bfield_measured_list, mwpc_dict, mu_dict),
                        open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
        except:
            print('Error on saving data json pickle')

        # Increment the current to scan
        if iteration_number >= (number_of_ekin_to_scan * repetition_number - 1):
            print(f"SCAN COMPLETED")
            iteration_number = (len(current1) - 2)
        # japc.stopSubscriptions()
        # japc.clearSubscriptions()
        # sys.exit("Scan completed")
        iteration_number += 1

        # Update the plot
        fig.canvas.draw()

        # Change the Energy
        print(f"Changing energy to: {round(energy[iteration_number])}")
        # new_energy = energy[iteration_number]
        # set_bfield_scan(new_energy)
        if ekin_old != energy[iteration_number]:
            set_bfield_scan(energy[iteration_number])
        if ekin_old == energy[iteration_number]:
            # print('DEBUG: Dont set because same energy')
            pass
        ekin_old = energy[iteration_number]
        # print("Waiting 10 seconds for energy to be set...")
        # time.sleep(10)
        # print("Done waiting")

        # Change the current in the QUAD
        print(f"logical.F61.QFN01/I_FUNC_LIST#value to: {round(current1, 3)}")
        print(f"logical.F61.QDN02/I_FUNC_LIST#value to: {round(current2, 3)}")
        print(f"logical.F61.QFN03/I_FUNC_LIST#value to: {round(current3, 3)}")
        print(f"logical.F61.QDN04/I_FUNC_LIST#value to: {round(current4, 3)}")
        print(f"logical.T8.QFN03/I_FUNC_LIST#value to: {round(current5, 3)}")
        print(f"logical.T8.QFN03/I_FUNC_LIST#value to: {round(current6, 3)}")
        print(f"logical.T8.QFN03/I_FUNC_LIST#value to: {round(current7, 3)}")
        print(f"logical.T8.QFN03/I_FUNC_LIST#value to: {round(current8, 3)}")
        # japc.rbacLogin()

        # Quadrupoles
        qfn01_i[0] = [[0, length_function_ms], [current1, current1]]
        qdn02_i[0] = [[0, length_function_ms], [current2, current2]]
        qfn03_i[0] = [[0, length_function_ms], [current3, current3]]
        qdn04_i[0] = [[0, length_function_ms], [current4, current4]]
        qfn05_i[0] = [[0, length_function_ms], [current5, current5]]
        qdn06_i[0] = [[0, length_function_ms], [current6, current6]]
        qdn07_i[0] = [[0, length_function_ms], [current7, current7]]
        qfn08_i[0] = [[0, length_function_ms], [current8, current8]]
        japc.setParam('logical.F61.QFN01/I_FUNC_LIST#value', qfn01_i)
        japc.setParam('logical.F61.QDN02/I_FUNC_LIST#value', qdn02_i)
        japc.setParam('logical.F61.QFN03/I_FUNC_LIST#value', qfn03_i)
        japc.setParam('logical.F61.QDN04/I_FUNC_LIST#value', qdn04_i)
        japc.setParam('logical.T8.QFN05/I_FUNC_LIST#value', qfn05_i)
        japc.setParam('logical.T8.QDN06/I_FUNC_LIST#value', qdn06_i)
        japc.setParam('logical.T8.QDN07/I_FUNC_LIST#value', qdn07_i)
        japc.setParam('logical.T8.QFN08/I_FUNC_LIST#value', qfn08_i)

        # Correctors
        dhz01_i[0] = [[0, length_function_ms], [dhz01_i_value, dhz01_i_value]]
        dvt01_i[0] = [[0, length_function_ms], [dvt01_i_value, dvt01_i_value]]
        dhz02_i[0] = [[0, length_function_ms], [dhz02_i_value, dhz02_i_value]]
        dvt02_i[0] = [[0, length_function_ms], [dvt02_i_value, dvt02_i_value]]
        dhz03_i[0] = [[0, length_function_ms], [dhz03_i_value, dhz03_i_value]]
        dvt03_i[0] = [[0, length_function_ms], [dvt03_i_value, dvt03_i_value]]
        japc.setParam('logical.F61.DHZ01/I_FUNC_LIST#value', dhz01_i)
        japc.setParam('logical.F61.DVT01/I_FUNC_LIST#value', dvt01_i)
        japc.setParam('logical.T8.DHZ02/I_FUNC_LIST#value', dhz02_i)
        japc.setParam('logical.T8.DVT02/I_FUNC_LIST#value', dvt02_i)
        japc.setParam('logical.T8.DHZ03/I_FUNC_LIST#value', dhz03_i)
        japc.setParam('logical.T8.DVT03/I_FUNC_LIST#value', dvt03_i)

        # Dipoles
        bhz01_i[0] = [[0, length_function_ms], [bhz01_i_value, bhz01_i_value]]
        bhz02_i[0] = [[0, length_function_ms], [bhz02_i_value, bhz02_i_value]]
        bhz03_i[0] = [[0, length_function_ms], [bhz03_i_value, bhz03_i_value]]
        bhz04_i[0] = [[0, length_function_ms], [bhz04_i_value, bhz04_i_value]]
        bhz05_i[0] = [[0, length_function_ms], [bhz05_i_value, bhz05_i_value]]
        japc.setParam('logical.F61.BHZ01/I_FUNC_LIST#value', bhz01_i)
        japc.setParam('logical.F61.BHZ02/I_FUNC_LIST#value', bhz02_i)
        japc.setParam('logical.T8.BHZ03/I_FUNC_LIST#value', bhz03_i)
        japc.setParam('logical.T8.BHZ04/I_FUNC_LIST#value', bhz04_i)
        japc.setParam('logical.T8.BHZ05/I_FUNC_LIST#value', bhz05_i)

        try:
            # Save figure
            fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                        transparent=False,
                        bbox_inches='tight');
        except:
            print('error on saving image')

        print("waiting for next shot")
    else:
        print("Skipping shot due to low intensity on BCT")


# Everytime this value changes we run the callback function
print("subscribing")
japc.subscribeParam("F61.QFN01/MEAS.PULSE", myCallback, getHeader=True)
japc.startSubscriptions()
fig.show()