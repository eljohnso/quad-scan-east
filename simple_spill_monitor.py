import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import pandas as pd
import datetime

time_at_script_start = datetime.datetime.now()

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")


fig, ax = plt.subplots(tight_layout=True)
fig.canvas.manager.set_window_title(f'Spill monitor started at: {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')
fig.suptitle(f'Spill monitor {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')


def myCallback(parameterName, newValue):

    ax.clear()
    fig.suptitle(datetime.datetime.now().strftime("%Y %m %d %Hh%Mm%Ss"))

    t = np.linspace(0, 1024/1.9502, len(newValue))

    signal_1000 = newValue
    signal_1001 = japc.getParam("BXSCINT_1001/Acquisition#countArray")

    d = {'t': t, 'signal_1000': signal_1000, 'signal_1001': signal_1001}
    df = pd.DataFrame(data=d)

    ax.plot(df.t, df.signal_1000, color="b", alpha=0.3, label="raw 1000")
    ax.plot(df.t, df.signal_1001, color="r", alpha=0.3, label="raw 1001")

    window = 10
    df2 = df.rolling(window=window).mean()
    ax.plot(df.t, df2.signal_1000, color="b", alpha=1., label=f"rolling mean window = {window} 1000")
    ax.plot(df.t, df2.signal_1001, color="r", alpha=1., label=f"rolling mean window = {window} 1001")

    ax.set_xlabel("Time [ms?]")
    ax.set_ylabel("Amplitude [arb.]")
    ax.legend()
    fig.canvas.draw() # Update the plot

japc.subscribeParam("BXSCINT_1000/Acquisition#countArray", myCallback)
japc.startSubscriptions()
fig.show()
