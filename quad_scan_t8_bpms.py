import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.dates as mdates
from matplotlib import cm
import datetime
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
import skimage
from skimage import filters
import time
import itertools
import pickle
import warnings
# warnings.filterwarnings("ignore")
from scipy.fft import fft, fftfreq
from scipy.fft import rfft, rfftfreq

time_at_script_start = datetime.datetime.now()

print(f'''
Quadrupole scan with filters
{time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}                                                                                                                                                                                     
''')

# Start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=False)
japc.rbacLogin()
user = "CPS.USER.MD3"
East_user = "CPS.USER.MD3"
japc.setSelector(user)

# BCT in the PSB to skip low intenstiy shots
bct = "EE.BCT10/Acquisition#totalIntensitySingle"
bct_threshold = 1
print(f"bct threshold LOW {bct_threshold}")
PSB_user = "LEI.USER.MDNOM"

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("quad_scan_t8_ion_data",
                                                        "quad_scan_t8_ion")

# Data we want to save
timestamp_list = []
intensity_list = []

converter_name_list = ["F61.QFN01/MEAS.PULSE#VALUE",
                       "F61.QDN02/MEAS.PULSE#VALUE",
                       "F61.QFN03/MEAS.PULSE#VALUE",
                       "F61.QDN04/MEAS.PULSE#VALUE",
                       "T8.QFN05/MEAS.PULSE#VALUE",
                       "T8.QDN06/MEAS.PULSE#VALUE",
                       "T8.QDN07/MEAS.PULSE#VALUE",
                       "T8.QFN08/MEAS.PULSE#VALUE",
                       ]
converter_dict = {converter_name: [] for converter_name in converter_name_list}

logical_k_list = ["logical.F61.QFN01/K_FUNC_LIST#value",
                  "logical.F61.QDN02/K_FUNC_LIST#value",
                  "logical.F61.QFN03/K_FUNC_LIST#value",
                  "logical.F61.QDN04/K_FUNC_LIST#value", ]
logical_k_dict = {logical_k: [] for logical_k in logical_k_list}

logicalt8_k_list = ["logical.T8.QFN05/K_FUNC_LIST#value",
                    "logical.T8.QDN06/K_FUNC_LIST#value",
                    "logical.T8.QDN07/K_FUNC_LIST#value",
                    "logical.T8.QFN08/K_FUNC_LIST#value", ]
logicalt8_k_dict = {logicalt8_k: [] for logicalt8_k in logicalt8_k_list}

logical_i_list = ["logical.F61.QFN01/I_FUNC_LIST#value",
                  "logical.F61.QDN02/I_FUNC_LIST#value",
                  "logical.F61.QFN03/I_FUNC_LIST#value",
                  "logical.F61.QDN04/I_FUNC_LIST#value", ]

logical_i_dict = {logical_i: [] for logical_i in logical_i_list}

current_oasis_list = ['F61.QFN01/LOG.OASIS.I_MEAS#DATA',
                      'F61.QDN02/LOG.OASIS.I_MEAS#DATA',
                      'F61.QFN03/LOG.OASIS.I_MEAS#DATA',
                      'F61.QDN04/LOG.OASIS.I_MEAS#DATA',
                      'F61.QFN05/LOG.OASIS.I_MEAS#DATA',
                      'F61.QDN06/LOG.OASIS.I_MEAS#DATA',
                      'F61.QDN07/LOG.OASIS.I_MEAS#DATA',
                      'F61.QFN08/LOG.OASIS.I_MEAS#DATA',]
current_oasis_dict = {current_oasis: [] for current_oasis in current_oasis_list}

bpm_list = ['PS-LOG-BPM-IRRAD-UCAP_BPM_01/Positions',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_02/Positions',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_03/Positions',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_04/Positions', ]
bpm_dict = {bpm: [] for bpm in bpm_list}

bpm_profiles_list = ['PS-LOG-BPM-IRRAD-UCAP_BPM_01/ProfilesAcquisition',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_02/ProfilesAcquisition',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_03/ProfilesAcquisition',
            'PS-LOG-BPM-IRRAD-UCAP_BPM_04/ProfilesAcquisition', ]
bpm_profiles_dict = {bpm_profiles: [] for bpm_profiles in bpm_profiles_list}

blm_list = ['F61.BLM008/Acquisition#lossBeamPresence',
            'F61.BLM027/Acquisition#lossBeamPresence',
            'F62.BLM002/Acquisition#lossBeamPresence',
            'F63.BLM003/Acquisition#lossBeamPresence',
            'T08.BLM006/Acquisition#lossBeamPresence',
            'T08.BLM023/Acquisition#lossBeamPresence',
            'T08.BLM045/Acquisition#lossBeamPresence',
            'T08.BLM060/Acquisition#lossBeamPresence',
            'T08.BLM067/Acquisition#lossBeamPresence',]
blm_dict = {blm: [] for blm in blm_list}

def blm_function(parameterName, newValue, header):
    global blm_dict
    blm_dict[parameterName].append(newValue)
    return

xsec_list = ['F61.XSEC023-I1/Acquisition#semNormData',
             'F61.XSEC023-I2/Acquisition#semNormData',
             'T08.XSEC070-I/Acquisition#semNormData',
             'T08.XSEC094-I/Acquisition#semNormData',]
xsec_dict = {xsec: [] for xsec in xsec_list}

def xsec_function(parameterName, newValue, header):
    global xsec_dict
    xsec_dict[parameterName].append(newValue)
    return

mwpc_list = ['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainProfilesAcquisition',
            'PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions',
            'PS-LOG-MWPC-UCAP_IRRAD_2080/HighGainProfilesAcquisition',
            'PS-LOG-MWPC-UCAP_IRRAD_2080/HighGainPositions', ]
mwpc_dict = {mwpc: [] for mwpc in mwpc_list}

blm_st_list = ['F61.BLM008-ST/Samples', ]
blm_st_dict = {blm: [] for blm in blm_st_list}

japc.setSelector(user)
print(80 * "_" + "\n")

print("Starting SCAN\n")
iteration_number = 0

qfn01_k = japc.getParam('logical.F61.QFN01/K_FUNC_LIST#value')
qdn02_k = japc.getParam('logical.F61.QDN02/K_FUNC_LIST#value')
qfn03_k = japc.getParam('logical.F61.QFN03/K_FUNC_LIST#value')
qdn04_k = japc.getParam('logical.F61.QDN04/K_FUNC_LIST#value')
qfn05_k = japc.getParam('logical.T8.QFN05/K_FUNC_LIST#value')
qdn06_k = japc.getParam('logical.T8.QDN06/K_FUNC_LIST#value')
qdn07_k = japc.getParam('logical.T8.QDN07/K_FUNC_LIST#value')
qfn08_k = japc.getParam('logical.T8.QFN08/K_FUNC_LIST#value')

# Optics file
optics_file = pickle.load( open( "optics_quadrupole_strength_t8.pickle", "rb" ) )
print(optics_file.name)

# optics_name = "fitted_params_parallel_12_mm"
# optics_name = "fitted_params_focal_mwpc_constrained"
optics_name = "operational_settings"

# Max currents: Q74L is +-733, Q120C is +-537, QFL is +-421
number_of_k_to_scan = 10
k1 = np.linspace(optics_file.loc[optics_file.name == optics_name].qfn01, optics_file.loc[optics_file.name == optics_name].qfn01, number_of_k_to_scan)  # init 0.47969
k2 = np.linspace(-optics_file.loc[optics_file.name == optics_name].qdn02, -optics_file.loc[optics_file.name == optics_name].qdn02, number_of_k_to_scan)  # init -0.1729
k3 = np.linspace(optics_file.loc[optics_file.name == optics_name].qfn03, optics_file.loc[optics_file.name == optics_name].qfn03, number_of_k_to_scan)  # init 0.19864
k4 = np.linspace(-optics_file.loc[optics_file.name == optics_name].qdn04, -optics_file.loc[optics_file.name == optics_name].qdn04, number_of_k_to_scan)  # init 0.19864
k5 = np.linspace(optics_file.loc[optics_file.name == optics_name].qfn05, optics_file.loc[optics_file.name == optics_name].qfn05, number_of_k_to_scan)  # init 0.19864
k6 = np.linspace(-optics_file.loc[optics_file.name == optics_name].qdn06, -optics_file.loc[optics_file.name == optics_name].qdn06, number_of_k_to_scan)  # init 0.19864
k7 = np.linspace(-optics_file.loc[optics_file.name == optics_name].qdn07, -optics_file.loc[optics_file.name == optics_name].qdn07, number_of_k_to_scan)  # init 0.19864
k8 = np.linspace(optics_file.loc[optics_file.name == optics_name].qfn08, optics_file.loc[optics_file.name == optics_name].qfn08, number_of_k_to_scan)  # init 0.19864
#If you want to scan a quadrupole and overwrite the optics file
k1 = np.linspace([0.4796], [0.4796], number_of_k_to_scan) # for MWPC scan
k2 = np.linspace([-0.21], [-0.21], number_of_k_to_scan) # for MWPC scan
k3 = np.linspace([0.19864], [0.19864], number_of_k_to_scan) # for MWPC scan
k4 = np.linspace([-0.09034], [-0.09034], number_of_k_to_scan) # for MWPC scan
k5 = np.linspace([0.19597], [0.19597], number_of_k_to_scan) # for MWPC scan
k6 = np.linspace([-0.194498], [-0.194498], number_of_k_to_scan) # for MWPC scan
k7 = np.linspace([-0.05], [-0.11], number_of_k_to_scan)  # for MWPC scan
k8 = np.linspace([0.0689891], [0.0689891], number_of_k_to_scan)
quadrupole_scanned = k7.copy()
# Statistics
repetition_number = 1
k1 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in k1))
k2 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in k2))
k3 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in k3))
k4 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in k4))
k5 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in k5))
k6 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in k6))
k7 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in k7))
k8 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in k8))

# japc.rbacLogin()
qfn01_k[0] = [[0, 540], [k1[0][0], k1[0][0]]]
qdn02_k[0] = [[0, 540], [k2[0][0], k2[0][0]]]
qfn03_k[0] = [[0, 540], [k3[0][0], k3[0][0]]]
qdn04_k[0] = [[0, 540], [k4[0][0], k4[0][0]]]
qfn03_k[0] = [[0, 540], [k5[0][0], k5[0][0]]]
qfn03_k[0] = [[0, 540], [k6[0][0], k6[0][0]]]
qfn03_k[0] = [[0, 540], [k7[0][0], k7[0][0]]]
qfn03_k[0] = [[0, 540], [k8[0][0], k8[0][0]]]
japc.setParam('logical.F61.QFN01/K_FUNC_LIST#value', qfn01_k)
japc.setParam('logical.F61.QDN02/K_FUNC_LIST#value', qdn02_k)
japc.setParam('logical.F61.QFN03/K_FUNC_LIST#value', qfn03_k)
japc.setParam('logical.F61.QDN04/K_FUNC_LIST#value', qdn04_k)
japc.setParam('logical.T8.QFN05/K_FUNC_LIST#value', qfn05_k)
japc.setParam('logical.T8.QDN06/K_FUNC_LIST#value', qdn06_k)
japc.setParam('logical.T8.QDN07/K_FUNC_LIST#value', qdn07_k)
japc.setParam('logical.T8.QFN08/K_FUNC_LIST#value', qfn08_k)

x_axis = []

fig, ax = plt.subplots(3, 5, figsize=(12, 8), tight_layout=True)

ax_V_sigma = ax[1, 1].twinx()
fig.canvas.manager.set_window_title(
    f'Quadrupole Scan started at: {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
fig.suptitle(f'Quadrupole scan BPMs {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
converter_dict_animation = {converter_name: [] for converter_name in converter_name_list}

##################
# Callback function, gets called everytime the value we're subscribed to gets updated
##################
def myCallback(parameterName, newValue, header):
    # Check if BCT in the PSB is above threshold
    bct_intensity = japc.getParam(bct, timingSelectorOverride=PSB_user)
    if bct_intensity > bct_threshold:
        ax[0, 0].clear()
        ax[0, 1].clear()
        ax[0, 2].clear()
        ax[0, 3].clear()
        ax[0, 4].clear()

        ax[1, 0].clear()
        ax[1, 1].clear()
        ax[1, 2].clear()
        ax[1, 3].clear()
        ax[1, 4].clear()

        ax[2, 0].clear()
        ax[2, 1].clear()
        ax[2, 2].clear()
        ax[2, 3].clear()
        ax[2, 4].clear()



        time = datetime.datetime.now()
        fig.suptitle(time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss"))
        print(f"New Shot for {parameterName} at {time.hour}h{time.minute}:{time.second}")
        global iteration_number
        print(f"Iteration number: {iteration_number}/{number_of_k_to_scan * repetition_number}")
        x_axis.append(iteration_number)
        # Append timestamp
        timestamp_list.append(time)

        # Append intensity
        intensity = japc.getParam("PR.BCT/HotspotIntensity#dcBefEje2", timingSelectorOverride=East_user)
        intensity_list.append(intensity)

        # Append all new currents for the new shot
        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict[converter_name].append(current)

        # Append all new logical k for the new shot
        for logical_k_name in logical_k_list:
            logical_k = japc.getParam(logical_k_name)
            logical_k_dict[logical_k_name].append(logical_k)

        # Append all new logical k for the new shot T8
        japc.setSelector(user)
        for logicalt8_k_name in logicalt8_k_list:
            logicalt8_k = japc.getParam(logicalt8_k_name)
            logicalt8_k_dict[logicalt8_k_name].append(logicalt8_k)
        japc.setSelector(user)

        # Append all new logical i for the new shot
        for logical_i_name in logical_i_list:
            logical_i = japc.getParam(logical_i_name)
            logical_i_dict[logical_i_name].append(logical_i)

        # Append all new current oasis for the new shot
        try:
            for current_oasis_name in current_oasis_list:
                current_oasis = japc.getParam(current_oasis_name)
                current_oasis_dict[current_oasis_name].append(current_oasis)
        except:
            print('failed to get oasis current')
            pass

        # Append all new bpm data
        bpm_number = 0
        for bpm_name in bpm_list:
            try:
                bpm_data = japc.getParam(bpm_name)
                bpm_dict[bpm_name].append(bpm_data)
                x = []
                H = []
                V = []
                for i in range(len(bpm_dict[bpm_name])):
                    x.append(quadrupole_scanned[i])
                    H.append(bpm_dict[bpm_name][i]['HSigma'])
                    V.append(bpm_dict[bpm_name][i]['VSigma'])
                ax[1, bpm_number].plot(x, H, marker = "x", color='b',
                                 label="$\sigma_{H}$")
                ax[1, bpm_number].plot(x, V, marker = "x", color='r',
                                 label="$\sigma_{V}$")
                ax[1, bpm_number].set_title(f"BPM{bpm_number+1}")
                ax[1, bpm_number].legend()
                ax[1, bpm_number].set_ylim(0,20)
                bpm_number +=1
            except:
                print("failed to plot bpm")
                pass

        # Append all new bpm profiles data
        bpm_number = 0
        for bpm_profiles_name in bpm_profiles_list:
            try:
                bpm_profiles_data = japc.getParam(bpm_profiles_name)
                bpm_profiles_dict[bpm_profiles_name].append(bpm_profiles_data)

                x = bpm_profiles_dict[bpm_profiles_name][-1]['HRawProfile'][0]
                y = bpm_profiles_dict[bpm_profiles_name][-1]['HRawProfile'][1]
                ax[2, bpm_number].plot(x,y, marker = "x", linestyle = 'None', color='b',
                                 label="$\sigma_{H}$")
                x = bpm_profiles_dict[bpm_profiles_name][-1]['HFitProfile'][0]
                y = bpm_profiles_dict[bpm_profiles_name][-1]['HFitProfile'][1]
                ax[2, bpm_number].plot(x, y, color='b')
                x = bpm_profiles_dict[bpm_profiles_name][-1]['VRawProfile'][0]
                y = bpm_profiles_dict[bpm_profiles_name][-1]['VRawProfile'][1]
                ax[2, bpm_number].plot(x,y, marker = "x", linestyle = 'None', color='r',
                                 label="$\sigma_{V}$")
                x = bpm_profiles_dict[bpm_profiles_name][-1]['VFitProfile'][0]
                y = bpm_profiles_dict[bpm_profiles_name][-1]['VFitProfile'][1]
                ax[2, bpm_number].plot(x, y, color='r')
                ax[2, bpm_number].set_title(f"BPM{bpm_number+1}")
                ax[2, bpm_number].legend()
                bpm_number +=1
            except:
                print("failed to plot bpm profiles")
                pass

        # Beam loss Monitors
        global blm_dict
        for blm_name in blm_list:
            try:
                ax[0,1].bar(str(blm_name[0:10]), blm_dict[blm_name][-3], ls = "dotted", alpha=0.33, fill=False, color="k")
            except:
                pass
            try:
                ax[0,1].bar(str(blm_name[0:10]), blm_dict[blm_name][-2], ls ="dashed", alpha=0.5, fill=False, color="k")
            except:
                pass
            try:
                ax[0,1].bar(str(blm_name[0:10]), blm_dict[blm_name][-1], alpha=1.0, fill=False, color="k")
            except:
                pass
        ax[0, 1].set_title("BLM")
        ax[0, 1].tick_params(axis='x', labelrotation=90, labelsize=8)
        ax[0, 1].set_ylim(0, 10)

        # XSEC
        global xsec_dict
        for xsec_name in xsec_list:
            try:
                ax[0, 2].bar(str(xsec_name[0:10]), xsec_dict[xsec_name][-3], ls = "dotted", alpha=0.33, fill=False, color="k")
            except:
                pass
            try:
                ax[0, 2].bar(str(xsec_name[0:10]), xsec_dict[xsec_name][-2], ls ="dashed", alpha=0.5, fill=False, color="k")
            except:
                pass
            try:
                ax[0,2].bar(str(xsec_name[0:10]), xsec_dict[xsec_name][-1], alpha=1.0, fill=False, color="k")
            except:
                pass
        ax[0, 2].set_title("XSEC")
        ax[0, 2].tick_params(axis='x', labelrotation=90, labelsize=8)
        ax[0, 2].set_ylim(0, 1e12)

        # Append all new mwpc for the new shot
        try:
            for mwpc_name in mwpc_list:
                mwpc = japc.getParam(mwpc_name)
                mwpc_dict[mwpc_name].append(mwpc)
        except:
            print('failed to get mwpc current')
            pass

        # Append all new blm for the new shot
        try:
            for blm_st_name in blm_st_list:
                blm_st = japc.getParam(blm_st_name)
                blm_st_dict[blm_st_name].append(blm_st)
        except:
            print('failed to get mwpc current')
            pass

        try:
            x = []
            H = []
            V = []
            for i in range(len(mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions'])):
                x.append(quadrupole_scanned[i])
                H.append(mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions'][i]['HSigma'])
                V.append(mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions'][i]['VSigma'])
            ax[1,4].plot(x, H, marker="x", color='b', label="$\sigma_{H}$")
            ax[1,4].plot(x, V, marker="x", color='r', label="$\sigma_{V}$")
            ax[1,4].set_xlabel("Quadrupole being scanned K")
            ax[1,4].set_ylabel("$\sigma$")
            ax[1,4].legend()
        except:
            print ('failed to plot mwpc')
            pass

        try:
            xH = mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainProfilesAcquisition'][-1]['HRawProfile'][0]
            H = mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainProfilesAcquisition'][-1]['HRawProfile'][1]
            xV = mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainProfilesAcquisition'][-1]['VRawProfile'][0]
            V = mwpc_dict['PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainProfilesAcquisition'][-1]['VRawProfile'][1]
            ax[2,4].plot(xH, H, marker="x", color='b', label="H")
            ax[2,4].plot(xV, V, marker="x", color='r', label="V")
            ax[2,4].set_xlabel("Position")
            ax[2,4].set_ylabel("Amplitude [arb.]")
            ax[2,4].legend(loc="upper right")
            ax[2,4].set_title("MWPC")
        except:
            print ('failed to plot mwpc raw profile')
            pass

        try:
            data = blm_st_dict['F61.BLM008-ST/Samples'][-1]['samples']
            blm_signal = np.ones(len(data))
            for i in range(len(data)):
                if i > 1:
                    blm_signal[i] = data[i] - data[i - 1]
                else:
                    blm_signal[i] = data[i]
            t = np.arange(0, len(blm_signal), 1)
            range_start = 1300 # ms
            range_end = 1680
            #ax1 = ax[0,3].twinx()
            ax[0,3].plot(t[range_start:range_end], blm_signal[range_start:range_end], color='k', label="signal")
            #ax1.plot(t[range_start:range_end], data[range_start:range_end], color='k', label="BLM008-ST Raw")
            ax[0,3].set_xlabel("Time [ms]")
            ax[0,3].set_ylabel("Amplitude [arb.]")
            ax[0,3].legend(loc="upper left")
            ax[0,3].set_title("BLM008-ST")
        except:
            print ('failed to plot BML008')
            pass

        try:
            yf = rfft(blm_signal[range_start:range_end])
            xf = rfftfreq(len(blm_signal[range_start:range_end]), 0.001)
            ax[0,4].plot(xf, np.abs(yf), color='k', label="FFT")
            ax[0,4].set_xlabel("Freq [Hz]")
            ax[0,4].set_ylabel("Amplitude [arb.]")
            ax[0,4].legend(loc="upper left")
            ax[0,4].set_title("BLM008-ST FFT")
        except:
            print ('failed to plot FFT of BML008')
            pass


        for converter_name in converter_name_list:
            current = japc.getParam(converter_name, )
            converter_dict_animation[converter_name].append(current)
            ax[0, 0].plot(timestamp_list, converter_dict_animation[converter_name], marker="o", ms=2, label=converter_name[0:9])
            ax[0, 0].set_xlabel("Local Time")
            ax[0, 0].set_ylabel("Current [A]")
            ax[0, 0].set_title("Converters")
            ax[0, 0].legend(fontsize=6)
            ax[0, 0].fmt_xdata = mdates.DateFormatter('%Y-%m-%d')  # Prettier date time
            ax[0, 0].tick_params('x', labelrotation=90)

        try:
            # JSON data dump
            with open(root_folder + "/" + folder_name + "/" + folder_name + ".json", 'w') as outfile:
                outfile.write(json.dumps((timestamp_list, converter_dict, logical_k_dict,
                                          intensity_list, logical_i_dict, current_oasis_dict, logicalt8_k_dict,
                                          bpm_dict, bpm_profiles_dict, blm_dict, xsec_dict, mwpc_dict, blm_st_dict),
                                         cls=myJSONEncoder))

            # Pickle data dump
            pickle.dump((timestamp_list, converter_dict, logical_k_dict, intensity_list,
                         logical_i_dict, current_oasis_dict, logicalt8_k_dict, bpm_dict, bpm_profiles_dict, blm_dict, xsec_dict, mwpc_dict, blm_st_dict),
                        open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
        except:
            print('Error on saving data json or pickle')

        # Increment the current to scan
        if iteration_number >= (number_of_k_to_scan * repetition_number - 1):
            print(f"SCAN COMPLETED")
            iteration_number = (len(k1) - 2)
        # japc.stopSubscriptions()
        # japc.clearSubscriptions()
        # sys.exit("Scan completed")
        iteration_number += 1

        # Update the plot
        fig.canvas.draw()

        # Change the current in the QUAD
        print("Changing quadrupoles to:")
        print(f"logical.F61.QFN01/K_FUNC_LIST#value to: {round(k1[iteration_number][0], 3)}")
        print(f"logical.F61.QDN02/K_FUNC_LIST#value to: {round(k2[iteration_number][0], 3)}")
        print(f"logical.F61.QFN03/K_FUNC_LIST#value to: {round(k3[iteration_number][0], 3)}")
        print(f"logical.F61.QDN04/K_FUNC_LIST#value to: {round(k4[iteration_number][0], 3)}")
        print(f"logical.T8.QFN05/K_FUNC_LIST#value to: {round(k5[iteration_number][0], 3)}")
        print(f"logical.T8.QDN06/K_FUNC_LIST#value to: {round(k6[iteration_number][0], 3)}")
        print(f"logical.T8.QDN07/K_FUNC_LIST#value to: {round(k7[iteration_number][0], 3)}")
        print(f"logical.T8.QFN08/K_FUNC_LIST#value to: {round(k8[iteration_number][0], 3)}")
        # japc.rbacLogin()
        qfn01_k[0] = [[0, 540], [k1[iteration_number][0], k1[iteration_number][0]]]
        qdn02_k[0] = [[0, 540], [k2[iteration_number][0], k2[iteration_number][0]]]
        qfn03_k[0] = [[0, 540], [k3[iteration_number][0], k3[iteration_number][0]]]
        qdn04_k[0] = [[0, 540], [k4[iteration_number][0], k4[iteration_number][0]]]
        qfn05_k[0] = [[0, 540], [k5[iteration_number][0], k5[iteration_number][0]]]
        qdn06_k[0] = [[0, 540], [k6[iteration_number][0], k6[iteration_number][0]]]
        qdn07_k[0] = [[0, 540], [k7[iteration_number][0], k7[iteration_number][0]]]
        qfn08_k[0] = [[0, 540], [k8[iteration_number][0], k8[iteration_number][0]]]
        japc.setParam('logical.F61.QFN01/K_FUNC_LIST#value', qfn01_k)
        japc.setParam('logical.F61.QDN02/K_FUNC_LIST#value', qdn02_k)
        japc.setParam('logical.F61.QFN03/K_FUNC_LIST#value', qfn03_k)
        japc.setParam('logical.F61.QDN04/K_FUNC_LIST#value', qdn04_k)
        japc.setParam('logical.T8.QFN05/K_FUNC_LIST#value', qfn05_k)
        japc.setParam('logical.T8.QDN06/K_FUNC_LIST#value', qdn06_k)
        japc.setParam('logical.T8.QDN07/K_FUNC_LIST#value', qdn07_k)
        japc.setParam('logical.T8.QFN08/K_FUNC_LIST#value', qfn08_k)

        try:
            # Save figure
            fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                        transparent=False,
                        bbox_inches='tight')
        except:
            print('error on saving image')
    else:
        print("Skipping shot due to low intensity on BCT")


# Everytime this value changes we run the callback function
print("subscribing")
japc.subscribeParam("PR.BCT/HotspotIntensity#dcBefEje2", myCallback, getHeader=True)
for blm_name in blm_list:
    japc.subscribeParam(blm_name, blm_function, getHeader=True)
for xsec_name in xsec_list:
    japc.subscribeParam(xsec_name, xsec_function, getHeader=True)
japc.startSubscriptions()
fig.show()


acq_number = 0