import pyjapc
import numpy as np
import functions as fs
import pickle

japc = pyjapc.PyJapc(noSet=False)
japc.setSelector("CPS.USER.MD6")
japc.rbacLogin()

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("dispersion_ion",
                                                        "dispersion_ion")

# Data we want to save
data_list = ["F61D.BTV010/Acquisition",
                       "F61D.BTV010/Image",]
data_dict = {data: [] for data in data_list}

bfield_array = np.linspace(2500, 2600, 10)

i=0
def myCallback(parameterName, newValue):

    global i

    # Append all new data for the new shot
    for data in data_list:
        data_inter = japc.getParam(data)
        data_dict[data].append(data_inter)

    bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")
    print(f'Actual B-field = {bfield[0]} [G]')

    new_bfield = bfield_array[i]
    bfield[0] = new_bfield
    print(f'New B-field = {bfield[0]} [G]')
    japc.setParam("PR.MPC/REF.PPPL#REF4", bfield, checkDims=False)

    # # Change the bending magnet
    # dump_bend = -66 * new_bfield / 2581
    # japc.setParam("F61.BHZ01.DUMP.A/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
    # print(f'Setting dump bend current to = {dump_bend} [A]')


    try:
        # Pickle data dump
        pickle.dump((data_dict, bfield_array), open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
    except:
        print('Error on saving data json pickle')
    i+=1

japc.subscribeParam("PR.BCT/HotspotIntensity#dcBefEje2", myCallback)
japc.startSubscriptions()