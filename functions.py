import datetime
import os
import numpy as np
from scipy.optimize import curve_fit

def create_folder(root_folder, title):
	time_at_script_start = datetime.datetime.now()
	time_title = time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")

	folder_name = title+"_"+time_title
	os.mkdir(root_folder+"/"+folder_name)
	print("Creating directory at:\n"+" "*4+root_folder+"/"+folder_name)
	print("")
	return root_folder, folder_name, time_title


def gaussian_function(x, a, I, mu, sig):
    return a + I / np.sqrt(2 * np.pi * sig ** 2) * np.exp(-(x - mu) ** 2 / 2. / sig ** 2)

def double_gaussian(x, *pars):
	offset = pars[-1]
	g1 = gaussian_function(x, pars[0], pars[1], pars[2], pars[3])
	g2 = gaussian_function(x, pars[4], pars[5], pars[6], pars[7])
	return g1 + g2 + offset

def triple_gaussian(x, *pars):
	offset = pars[-1]
	g1 = gaussian_function(x, pars[0], pars[1], pars[2], pars[3])
	g2 = gaussian_function(x, pars[4], pars[5], pars[6], pars[7])
	g3 = gaussian_function(x, pars[8], pars[5], pars[9], pars[10])
	return g1 + g2 + g3 + offset

def do_gaussian_fit(x,y):
    mu = np.average(x, weights=np.abs(y - np.min(y)))
    sigma = np.sqrt(np.average(x**2, weights=np.abs(y - np.min(y))) - mu**2)
    p0 = [y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma**2), mu, sigma]
    popt, pcov = curve_fit(gaussian_function, x, y, p0=p0, maxfev=1000) # maxfev is the number of tries it does the fit
    return popt, pcov

def do_double_gaussian_fit(x,y):
    mu = np.average(x, weights=np.abs(y - np.min(y)))
    sigma = np.sqrt(np.average(x**2, weights=np.abs(y - np.min(y))) - mu**2)
    p0 = [y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma**2), mu, sigma,
		  y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma**2), mu, sigma,
		  1]
    popt, pcov = curve_fit(double_gaussian, x, y, p0=p0, maxfev=10000) # maxfev is the number of tries it does the fit
    return popt, pcov

def do_triple_gaussian_fit(x,y):
    mu = np.average(x, weights=np.abs(y - np.min(y)))
    sigma = np.sqrt(np.average(x**2, weights=np.abs(y - np.min(y))) - mu**2)
    p0 = [y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma**2), mu, sigma,
		  y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma**2), mu, sigma,
		  y.min(), (np.max(y) - np.min(y)) * np.sqrt(2 * np.pi * sigma ** 2), mu, sigma,
		  1]
    popt, pcov = curve_fit(triple_gaussian, x, y, p0=p0, maxfev=10000) # maxfev is the number of tries it does the fit
    return popt, pcov


# Function for MADX

def beam_size(beta, dispersion, eps, dpp, n):
    """
    Simple calculation of beam size
    """
    beam = np.sqrt(eps*beta + (dpp*dispersion)**2)
    return beam*n


def draw_synoptic(ax, twiss, height):
	"""
    Simple function to draw a synoptic as boxes
    uses the keyword column of a dataframe containing twiss

    Parameters
    ----------
    ax : matplotlib.axes
        Axes where the synoptic is to be drawn

    twiss : DataFrame
        pandas dataframe containing the lattice and in
        in particular the S, L, K1L and KEYWORD columns

    with_cpymad : bool
        True if using cpymad provided twiss file

    """

	height = height
	text_rotation = 0

	for _, row in twiss.iterrows():

		if row['keyword'] == 'quadrupole':
			_ = ax.add_patch(
				mpl.patches.Rectangle(
					(row['s'] - row['l'], -height / 2), row['l'], height,
					facecolor='m', edgecolor='k', alpha=0.3))

			ax.annotate(row['keyword'],
						xy=(row["s"] - row["l"] / 2, height / 2), xycoords='data',
						xytext=(0, 0), textcoords='offset points',
						horizontalalignment='center', verticalalignment='bottom', rotation=text_rotation)

			ax.annotate(row['name'],
						xy=(row["s"] - row["l"] / 2, -height / 2), xycoords='data',
						xytext=(0, 0), textcoords='offset points',
						horizontalalignment='center', verticalalignment='top', rotation=text_rotation)

		elif (row['keyword'] == 'rbend' or
			  row['keyword'] == 'sbend'):
			_ = ax.add_patch(
				mpl.patches.Rectangle(
					(row['s'] - row['l'], -height / 2), row['l'], height,
					facecolor='b', edgecolor='k', alpha=0.3))

			ax.annotate(row['keyword'],
						xy=(row["s"] - row["l"] / 2, height / 2), xycoords='data',
						xytext=(0, 0), textcoords='offset points',
						horizontalalignment='center', verticalalignment='bottom', rotation=text_rotation)

			ax.annotate(row['name'],
						xy=(row["s"] - row["l"] / 2, -height / 2), xycoords='data',
						xytext=(0, 0), textcoords='offset points',
						horizontalalignment='center', verticalalignment='top')

		elif (row['keyword'] == 'monitor' and row['name'] != 'f61.bctf022:1' and row['name'] != 'f61.bcgaa023:1' and
			  row['name'] != 'f61.xsec023:1'):
			_ = ax.add_patch(
				mpl.patches.Rectangle(
					(row['s'] - row['l'], -height / 2), row['l'], height,
					facecolor='r', edgecolor='k', alpha=0.3))

			ax.annotate(row['keyword'],
						xy=(row["s"] - row["l"] / 2, height / 2), xycoords='data',
						xytext=(0, 0), textcoords='offset points',
						horizontalalignment='center', verticalalignment='bottom', rotation=text_rotation)

			ax.annotate(row['name'],
						xy=(row["s"] - row["l"] / 2, -height / 2), xycoords='data',
						xytext=(0, 0), textcoords='offset points',
						horizontalalignment='center', verticalalignment='top', rotation=text_rotation)


def interpolate_gradient(current, magnet_type):
	A_Q74L = [5.000978,
			  10.000304,
			  50.002003,
			  100.001404,
			  150.001373,
			  200.00238,
			  250.005432,
			  299.993652,
			  350.001648,
			  399.995728,
			  449.994446,
			  499.997375,
			  549.999878,
			  599.994873,
			  650.004211,
			  666.999146,
			  699.991455,
			  749.998169,
			  799.996643]

	T_Q74L = [0.2619,
			  0.492968,
			  2.358847,
			  4.710821,
			  7.068205,
			  9.426532,
			  11.780794,
			  14.131984,
			  16.4773,
			  18.814776,
			  21.133094,
			  23.416058,
			  25.617934,
			  27.638804,
			  29.43015,
			  29.981004,
			  30.987122,
			  32.347211,
			  33.545971]

	A_Q120C = [0.00,
			   50.00,
			   100.00,
			   150.00,
			   200.00,
			   250.00,
			   300.00,
			   350.00,
			   400.00,
			   450.00,
			   500.00,
			   550.00,
			   600.00,
			   650.00]

	T_Q120C = [0.00,
			   2.05,
			   4.12,
			   6.19,
			   8.26,
			   10.33,
			   12.39,
			   14.43,
			   16.44,
			   18.36,
			   20.14,
			   21.74,
			   23.01,
			   24.07]

	A_QFL = [0,
			 94.15,
			 145.76,
			 197.97,
			 250.74,
			 300.18,
			 350.7,
			 402,
			 451.15,
			 502.09,
			 551.47]

	T_QFL = [0,
			 4.86,
			 7.48,
			 10.158,
			 12.875,
			 15.377,
			 17.81,
			 20.045,
			 21.844,
			 23.217,
			 24.445]

	if magnet_type == "Q74L":
		measured_currents = A_Q74L
		measured_int_gradient = T_Q74L
	if magnet_type == "Q120C":
		measured_currents = A_Q120C
		measured_int_gradient = T_Q120C
	if magnet_type == "QFL":
		measured_currents = A_QFL
		measured_int_gradient = T_QFL

	# Make the curve symmetric
	if (current < 0):
		interpolated_int_gradient = -np.interp(-current, measured_currents, measured_int_gradient)
	else:
		interpolated_int_gradient = np.interp(current, measured_currents, measured_int_gradient)

	return interpolated_int_gradient