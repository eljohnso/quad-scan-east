import pyjapc
import matplotlib.pyplot as plt

japc = pyjapc.PyJapc(noSet=True)
user = "CPS.USER.EAST3"
japc.setSelector(user)

profile1 = japc.getParam("PS-LOG-BPM-IRRAD-UCAP_BPM_01/ProfilesAcquisition")
profile2 = japc.getParam("PS-LOG-BPM-IRRAD-UCAP_BPM_02/ProfilesAcquisition")
profile3 = japc.getParam("PS-LOG-BPM-IRRAD-UCAP_BPM_03/ProfilesAcquisition")
profile4 = japc.getParam("PS-LOG-BPM-IRRAD-UCAP_BPM_04/ProfilesAcquisition")

fig1, ax1 = plt.subplots()
ax1.plot(profile1["HRawProfile"][0], profile1["HRawProfile"][1], color="b", label="H raw")
ax1.plot(profile1["HFitProfile"][0], profile1["HFitProfile"][1], color="b", label="H fit")
ax1.plot(profile1["VRawProfile"][0], profile1["VRawProfile"][1], color="r", label="V raw")
ax1.plot(profile1["VFitProfile"][0], profile1["VFitProfile"][1], color="r", label="V fit")
ax1.set_title("BPM1")
ax1.legend()

fig2, ax2 = plt.subplots()
ax2.plot(profile2["HRawProfile"][0], profile2["HRawProfile"][1], color="b", label="H raw")
ax2.plot(profile2["HFitProfile"][0], profile2["HFitProfile"][1], color="b", label="H fit")
ax2.plot(profile2["VRawProfile"][0], profile2["VRawProfile"][1], color="r", label="V raw")
ax2.plot(profile2["VFitProfile"][0], profile2["VFitProfile"][1], color="r", label="V fit")
ax2.set_title("BPM2")
ax2.legend()

fig3, ax3 = plt.subplots()
ax3.plot(profile3["HRawProfile"][0], profile3["HRawProfile"][1], color="b", label="H raw")
ax3.plot(profile3["HFitProfile"][0], profile3["HFitProfile"][1], color="b", label="H fit")
ax3.plot(profile3["VRawProfile"][0], profile3["VRawProfile"][1], color="r", label="V raw")
ax3.plot(profile3["VFitProfile"][0], profile3["VFitProfile"][1], color="r", label="V fit")
ax3.set_title("BPM3")
ax3.legend()

fig4, ax4 = plt.subplots()
ax4.plot(profile4["HRawProfile"][0], profile4["HRawProfile"][1], color="b", label="H raw")
ax4.plot(profile4["HFitProfile"][0], profile4["HFitProfile"][1], color="b", label="H fit")
ax4.plot(profile4["VRawProfile"][0], profile4["VRawProfile"][1], color="r", label="V raw")
ax4.plot(profile4["VFitProfile"][0], profile4["VFitProfile"][1], color="r", label="V fit")
ax4.set_title("BPM4")
ax4.legend()
plt.show()