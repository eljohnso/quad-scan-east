import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import datetime
import os
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
import sys
plt.style.use('ggplot')

print(f'''
   ____                  _    _____                 
  / __ \                | |  / ____|                
 | |  | |_   _  __ _  __| | | (___   ___ __ _ _ __  
 | |  | | | | |/ _` |/ _` |  \___ \ / __/ _` | '_ \ 
 | |__| | |_| | (_| | (_| |  ____) | (_| (_| | | | |
  \___\_\\__,_|\__,_|\__,_| |_____/ \___\__,_|_| |_|
{datetime.datetime.now()}                                                                                                                                                                                     
''')

# We start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("quad_scan_east_dump_data","quad_scan_east_dump")

# Create empty list to be filled with data
timestamp_list = []

btv_name_list = ["F61D.BTV010/Acquisition"]
btv_dict = {btv_name: [] for btv_name in btv_name_list}

btv_name_image_list = ["F61D.BTV010/Image"]
btv_image_dict = {btv_name_image: [] for btv_name_image in btv_name_image_list}


converter_name_list = ["F61.QFN01/MEAS.PULSE#VALUE",
			"F61.QDN02/MEAS.PULSE#VALUE",
			"F61.QFN03/MEAS.PULSE#VALUE"]
converter_dict = {converter_name: [] for converter_name in converter_name_list}

# Initial CONVERTERS currents
print(80*"_")
print("Initial Converters settings:\n")
japc.setParam("F61.QFN01/REF.PULSE.AMPLITUDE#VALUE",620)
japc.setParam("F61.QDN02/REF.PULSE.AMPLITUDE#VALUE",404)
japc.setParam("F61.QFN03/REF.PULSE.AMPLITUDE#VALUE",378)
print(80*"_"+"\n")

# Initial BTV parameters
print(80*"_")
print("Initial BTV settings:\n")

japc = pyjapc.PyJapc(noSet=False)
japc.setSelector("CPS.USER.EAST3")
japc.setParam("F61D.BTV010/MultiplexedSetting#reqUserImageSelection", [True,True,True,True,True,True,True])
japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")
#japc.setParam("F61D.BTV010/Acquisition#cameraSwitch","ON")
#japc.setParam("F61D.BTV010/Acquisition#screenSelect","In")
#japc.setParam("F61.BTV012/Acquisition#cameraSwitch","OFF")
#japc.setParam("F61.BTV012/Acquisition#screenSelect","Out")
print(80*"_"+"\n")

print("Starting SCAN\n")
iteration_number = 0
converter_to_scan = "F61.QFN03/REF.PULSE.AMPLITUDE#VALUE"
logical_k_to_scan = "logical.F61.QFN03/K_FUNC_LIST#value"
current_to_scan = np.linspace(0,420,20)
k_to_scan = np.linspace(0.0,0.215,20)


##################
# Callback function, gets called everytime the value we're subscribed to gets updated
##################
def myCallback(parameterName, newValue, header):
	japc.setSelector("CPS.USER.EAST3")
	print(f"New value for {parameterName} is: {newValue}")
	global iteration_number
	print(f"Iteration number: {iteration_number}")

	# Set new current to the converter we scan
	#print(f"Setting {converter_to_scan} current to: {current_to_scan[iteration_number]} [A]")
	#japc.setParam(converter_to_scan,current_to_scan[iteration_number])
	
	# Set new K1 to converter we scan
	print(f"Setting {logical_k_to_scan} k to: {current_to_scan[iteration_number]} [A]")
	my_array = [np.array([[0, 429], [k_to_scan[iteration_number], k_to_scan[iteration_number]]])]
	japc.setParam(logical_k_to_scan, my_array)

	# Append timestamp
	timestamp_list.append(datetime.datetime.now())
	
	# Append all new btv data for the new shot
	for btv_name in btv_name_list:
		btv_data = japc.getParam(btv_name)
		btv_dict[btv_name].append(btv_data)

	# Append btv image to the new shot
	for btv_name_image in btv_name_image_list:
		btv_data_image = japc.getParam(btv_name_image)
		btv_image_dict[btv_name_image].append(btv_data_image)

		fig_im, ax_im = plt.subplots(tight_layout=True)
		print (len(btv_data_image["imageSet"][0]))
		def animate_func(i):
		    image = btv_data_image["imageSet"][0]
		    reshaped_image = image.reshape(264,-1)
		    ax_im.imshow(reshaped_image, interpolation='nearest')
		    ax_im.set_title(f"Acq: {i}")
		    return

		anim = animation.FuncAnimation(
				               fig_im, 
				               animate_func, 
				               frames = 4,
				               interval = 1000, # in ms
				               )
		anim.save(root_folder+"/"+folder_name+"/btv_anim_auto.gif", fps=4)

	# Append all new currents for the new shot
	for converter_name in converter_name_list:
		current = japc.getParam(converter_name)
		converter_dict[converter_name].append(current)

	# JSON data dump
	with open(root_folder+"/"+folder_name+"/"+folder_name+".json",'w') as outfile:
		outfile.write(json.dumps((timestamp_list, btv_dict, converter_dict), cls=myJSONEncoder))

	# Pickle data dump
	pickle.dump((timestamp_list, btv_dict, converter_dict), open(root_folder+"/"+folder_name+"/"+folder_name+'.p', 'wb'))

	# Increment the current to scan
	if iteration_number >= (len(current_to_scan)-1):
		print(f"Scan completed")
		iteration_number = (len(current_to_scan)-2)		
		#japc.stopSubscriptions()
		#japc.clearSubscriptions()
		#sys.exit("Scan completed")
	iteration_number+=1



# Everytime this value changes we run the callback function
japc.subscribeParam("EA.BLM/Acquisition#lossBeamPresence", myCallback, getHeader=True)
japc.startSubscriptions()

##################
# Animation part #
##################
converter_dict_animation = {converter_name: [] for converter_name in converter_name_list}
time_list = []

def force_update_plot(i):
	### First Plot with Converter current
	ax[0,0].clear()

	# Time
	time = datetime.datetime.now()
	time_list.append(time)
	
	# Converter
	for converter_name in converter_name_list:
		current = japc.getParam(converter_name)
		converter_dict_animation[converter_name].append(current)
		ax[0,0].plot(time_list,converter_dict_animation[converter_name], marker="o", label=converter_name)
	
	ax[0,0].set_xlabel("Local Time")
	ax[0,0].set_ylabel("Current [A]")
	ax[0,0].set_title("Converters")
	ax[0,0].legend()

	### Second plot with BTV data
	ax[0,1].clear()
	for btv_name in btv_name_list:
		btv_data = japc.getParam(btv_name)
		for image_index in range(len(btv_data["projPositionSet1"])):
			Hx = btv_data["projPositionSet1"][image_index]
			Hy = btv_data["projDataSet1"][image_index]
			ax[0,1].plot(Hx, Hy, label= f"H idx{image_index}")
			# Fit gaussian H
			try:
				popt = do_gaussian_fit(Hx,Hy)
				x_mean = popt[2]
				ax[0,1].plot(Hx, gaussian_function(Hx, *popt), label = "H-fit")
			except:
				#print("failed fit")
				pass

		for image_index in range(len(btv_data["projPositionSet2"])):
			Vx = btv_data["projPositionSet2"][image_index]
			Vy = btv_data["projDataSet2"][image_index]
			ax[0,1].plot(Vx, Vy, label= f"V idx{image_index}")
			# Fit gaussian V
			try:
				popt = do_gaussian_fit(Vx,Vy)
				y_mean = popt[2]
				ax[0,1].plot(x, gaussian_function(Vx, *popt), label = "H-fit")
			except:
				#print("failed fit")
				pass

		# Plot center of beam on third plot
		try:
			ax[1,1].scatter(x_mean, y_mean, label=f"{btv_name} center")
		except:
			pass


	ax[0,1].set_xlabel("H / V [mm]")
	ax[0,1].set_ylabel("Amplitude [arb.]")
	ax[0,1].set_title(str(btv_name_list[0]))
	ax[0,1].legend()

	ax[1,1].set_xlabel("H position [mm]")
	ax[1,1].set_ylabel("V position [mm]")
	ax[1,1].set_title("Mean beam position")
	

fig, ax = plt.subplots(2,2, tight_layout=True)
fig.canvas.manager.set_window_title('Quadrupole scan East Dump')
#ani = FuncAnimation(fig, force_update_plot, interval=100)
plt.show()
