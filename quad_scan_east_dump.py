import numpy as np
import pyjapc
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import datetime
import os
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
plt.style.use('ggplot')

# We start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.LHCIND2")

# Use a function to create a data folder
root_folder, folder_name, time_title = fs.create_folder("quad_scan_east_dump_data","quad_scan_east_dump")

timestamp_list = []
btv_name_list = ["F61.BTV012/Acquisition",
	"F61D.BTV010/Acquisition"]
btv_dict = {btv_name: [] for btv_name in btv_name_list}

converter_name_list = ["F61.QFN01/MEAS.PULSE#VALUE",
	"F61.QDN02/MEAS.PULSE#VALUE",
	"F61.QFN03/MEAS.PULSE#VALUE"]
converter_dict = {converter_name: [] for converter_name in converter_name_list}

##################
# Callback function, gets called everytime the value we're subscribed to gets updated
##################
def myCallback(parameterName, newValue):
	print(f"New value for {parameterName} is: {newValue}")

	timestamp_list.append(datetime.datetime.now())
	
	# Append all new btv data for the new shot
	for btv_name in btv_name_list:
		btv_data = japc.getParam(btv_name)
		btv_dict[btv_name].append(btv_data)

	# Append all new currents for the new shot
	for converter_name in converter_name_list:
		current = japc.getParam(converter_name)
		converter_dict[converter_name].append(current)

	# JSON data dump
	with open(root_folder+"/"+folder_name+"/"+folder_name+".json",'w') as outfile:
		outfile.write(json.dumps((timestamp_list, btv_dict, converter_dict), cls=myJSONEncoder))

	# Pickle data dump
	pickle.dump((timestamp_list, btv_dict, converter_dict), open(root_folder+"/"+folder_name+"/"+folder_name+'.p', 'wb'))

# Everytime this value changes we run the callback function
japc.subscribeParam("EA.BLM/Acquisition#lossBeamPresence", myCallback)
japc.startSubscriptions()

##################
# Animation part #
##################
QFN01_current_list = []
time_list = []

def force_update_plot(i):
	ax.clear()

	# Current
	current = japc.getParam("F61.QFN01/MEAS.PULSE#VALUE")
	QFN01_current_list.append(current)
	
	# Time
	time = datetime.datetime.now()
	time_list.append(time)

	# Plotting
	ax.plot(time_list,QFN01_current_list, marker="o")
	ax.set_xlabel("Time")
	ax.set_ylabel("Current [A]")
	ax.set_title("QFNO1 measured current")

fig, ax = plt.subplots(tight_layout=True)
fig.canvas.manager.set_window_title('Quadrupole scan East Dump')	
ani = FuncAnimation(fig, force_update_plot, interval=500)
plt.show()
