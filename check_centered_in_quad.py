import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.dates as mdates
from matplotlib import cm
import datetime
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
from scipy import stats

print(f'''
   _____           _               ____  
  / ____|         | |             / __ \ 
 | |     ___ _ __ | |_ _ __ ___  | |  | |
 | |    / _ \ '_ \| __| '__/ _ \ | |  | |
 | |___|  __/ | | | |_| | |  __/ | |__| |
  \_____\___|_| |_|\__|_|  \___|  \___\_\

{datetime.datetime.now()}                                                                                                                                                                                     
''')

# Start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=False)
japc.rbacLogin()
user = "CPS.USER.SFTPRO3"
East_user = "CPS.USER.SFTPRO3"
japc.setSelector(user)

# BCT in the PSB to skip low intenstiy shots
bct = "BTP.BCT10/Acquisition#totalIntensityCalcCh1"
bct_threshold = 5
PSB_user = "PSB.USER.MD4"

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("check_centered_in_quad_data", "check_centered_in_quad")

# Data we want to save
timestamp_list = []

btv_name_list = ["F61D.BTV010"]
btv_dict = {btv_name + "/Acquisition": [] for btv_name in btv_name_list}
btv_image_dict = {btv_name + "/Image": [] for btv_name in btv_name_list}


converter_name_list = ["F61.QFN03/MEAS.PULSE#VALUE"] # Only one Quadrupole at a time !
converter_dict = {converter_name: [] for converter_name in converter_name_list}

steerer_name_list = ["F61.DHZ01/MEAS.PULSE#VALUE"]
steerer_dict = {steerer_name: [] for steerer_name in steerer_name_list}

mu_dict = {"mu_h0": [], "mu_h1": [], "mu_h2": [], "mu_h3": [], "mu_h4": [], "mu_h5": [], "mu_h6": [],
              "mu_v0": [], "mu_v1": [], "mu_v2": [], "mu_v3": [], "mu_v4": [], "mu_v5": [], "mu_v6": []}
mu_err_dict = {"mu_h0": [], "mu_h1": [], "mu_h2": [], "mu_h3": [], "mu_h4": [], "mu_h5": [], "mu_h6": [],
              "mu_v0": [], "mu_v1": [], "mu_v2": [], "mu_v3": [], "mu_v4": [], "mu_v5": [], "mu_v6": []}

sigma_dict = {"sig_h0": [], "sig_h1": [], "sig_h2": [], "sig_h3": [], "sig_h4": [], "sig_h5": [], "sig_h6": [],
              "sig_v0": [], "sig_v1": [], "sig_v2": [], "sig_v3": [], "sig_v4": [], "sig_v5": [], "sig_v6": []}
sigma_err_dict = {"sig_h0": [], "sig_h1": [], "sig_h2": [], "sig_h3": [], "sig_h4": [], "sig_h5": [], "sig_h6": [],
                  "sig_v0": [], "sig_v1": [], "sig_v2": [], "sig_v3": [], "sig_v4": [], "sig_v5": [], "sig_v6": []}

# Initial BTV parameters
print(80 * "_")
print("Initial BTV settings:\n")
print("Selecting all images to acquire")
japc.setParam(btv_name_list[0] + "/MultiplexedSetting#reqUserImageSelection",
              [True, True, True, True, True, True, True])
japc.setSelector("")  # Following settings requires no selector
print("Turning on Camera Switch")
japc.setParam(btv_name_list[0] + "/Setting#screenSelect", 'FIRST')
print("Inserting Screen")
japc.setParam(btv_name_list[0] + "/Setting#cameraSwitch", 'CCD_ON_RAD_STDBY')
japc.setSelector(user)
print(80 * "_" + "\n")

print("Starting SCAN\n")
iteration_number = 0


Brho = 24*3.3356
quad_length = {
    "QFN01": 0.74,
    "QDN02": 1.2,
    "QFN03": 1.2,
}
# QFN01 -733 to 733 [A], type Q74L
# QDN02 -537 to 537 [A], type Q120C
# QFN03 -421 to 421 [A], type QFL

#logical_k_to_scan = "logical.F61.QFN03/K_FUNC_LIST#value"
quadrupole_to_scan = "F61.QFN03/REF.PULSE.AMPLITUDE#VALUE"
start_current = 100
end_current = 400 # Max currents: Q74L is +-733, Q120C is +-537, QFL is +-421
# start_k = fs.interpolate_gradient(start_current, "QFL")/(quad_length["QFN03"]*Brho)
# end_k = fs.interpolate_gradient(end_current, "QFL")/(quad_length["QFN03"]*Brho)
# k_to_scan = np.linspace(start_k, end_k, 3) # Max Ks: Q74L is 0.536, Q120C is 0.3588, QFL is 0.346
current_to_scan = np.linspace(start_current, end_current, 3)
# k_list = []
# k_dict = {"k1": []}


fig, ax = plt.subplots(2,2, figsize=(10, 8), tight_layout=True)
ax_V_sigma = ax[1,1].twinx()
fig.canvas.manager.set_window_title('Check Centered in Quad')
converter_dict_animation = {converter_name: [] for converter_name in converter_name_list}

# Initial image on the second window
btv_data_image = japc.getParam(btv_name_list[0] + "/Image", timingSelectorOverride=East_user)
number_of_acqu = len(btv_data_image["imageSet"])


##################
# Callback function, gets called everytime the value we're subscribed to gets updated
##################
def myCallback(parameterName, newValue, header):
    # Check if BCT in the PSB is above threshold
    bct_intensity = japc.getParam(bct, timingSelectorOverride=PSB_user)
    print (f"bct intensity = {round(bct_intensity,3)}")
    if bct_intensity > bct_threshold:
        ax[0, 0].clear()
        ax[1, 0].clear()
        time = datetime.datetime.now()
        print(f"New Shot for {parameterName} at {time.hour}h{time.minute}:{time.second}")
        global iteration_number
        print(f"Iteration number: {iteration_number}")

        # Append timestamp
        timestamp_list.append(time)

        # Set new K1 to converter we scan
        #print(f"Setting {logical_k_to_scan} k to: {round(k_to_scan[iteration_number],3)}")

        #my_array = [np.array([[0, 429], [k_to_scan[iteration_number], k_to_scan[iteration_number]]])]


        # ax[0, 1].plot(my_array[0][0], my_array[0][1], marker="o", label=f"k1 = {round(current_to_scan[iteration_number], 3)}")
        # ax[0, 1].set_xlabel("Time [ms]")
        # ax[0, 1].set_ylabel("K1")
        # ax[0, 1].set_title("Logical K")

        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict_animation[converter_name].append(current)



        for steerer_name in steerer_name_list:
            current = japc.getParam(steerer_name)
            steerer_dict[steerer_name].append(current)

            ax[0, 0].plot(timestamp_list, steerer_dict[steerer_name], marker="o", label=steerer_name)
            ax[0, 0].set_xlabel("Local Time")
            ax[0, 0].set_ylabel("Current [A]")
            ax[0, 0].set_title("Steerer")
            ax[0, 0].legend()
            ax[0, 0].fmt_xdata = mdates.DateFormatter('%Y-%m-%d')  # Prettier date time
            ax[0, 0].tick_params('x', labelrotation=45)

        # Append all new btv data (projections, etc) for the new shot
        for btv_name in btv_name_list:
            btv_data = japc.getParam(btv_name + "/Acquisition")
            btv_dict[btv_name + "/Acquisition"].append(btv_data)

            H_mu_list = []
            H_mu_err_list = []
            V_mu_list = []
            V_mu_err_list = []
            H_sigma_list = []
            H_sigma_err_list = []
            V_sigma_list = []
            V_sigma_err_list = []

            for i in range(len(btv_dict[btv_name + "/Acquisition"][-1]["positionSet1"])):
                # Horizontal Gaussian Fit
                Hx = btv_dict[btv_name + "/Acquisition"][-1]["projPositionSet1"][i]
                Hy = btv_dict[btv_name + "/Acquisition"][-1]["projDataSet1"][i]

                try:
                    popt, pcov = fs.do_gaussian_fit(Hx, Hy)
                    mu = popt[2]
                    H_mu_list.append(mu)
                    mu_err = pcov[2, 2] ** 0.5
                    H_mu_err_list.append(mu_err)
                    sigma = abs(popt[3])
                    H_sigma_list.append(sigma)
                    sigma_err = pcov[3, 3] ** 0.5
                    H_sigma_err_list.append(sigma_err)

                    mu_dict["mu_h"+str(i)].append(mu)
                    mu_err_dict["mu_h" + str(i)].append(mu_err)

                    sigma_dict["sig_h"+str(i)].append(sigma)
                    sigma_err_dict["sig_h" + str(i)].append(sigma_err)

                except:
                    # print ("failed to do the gaussian fit")
                    mu_dict["mu_h" + str(i)].append(np.nan)
                    mu_err_dict["mu_h" + str(i)].append(np.nan)

                    sigma_dict["sig_h" + str(i)].append(np.nan)
                    sigma_err_dict["sig_h" + str(i)].append(np.nan)
                    pass
                # Vertical Gaussian Fit
                Vx = btv_dict[btv_name + "/Acquisition"][-1]["projPositionSet2"][i]
                Vy = btv_dict[btv_name + "/Acquisition"][-1]["projDataSet2"][i]

                try:
                    popt, pcov = fs.do_gaussian_fit(Vx, Vy)
                    mu = popt[2]
                    V_mu_list.append(mu)
                    mu_err = pcov[2, 2] ** 0.5
                    V_mu_err_list.append(mu_err)
                    sigma = abs(popt[3])
                    V_sigma_list.append(sigma)
                    sigma_err = pcov[3, 3] ** 0.5
                    V_sigma_err_list.append(sigma_err)

                    mu_dict["mu_v" + str(i)].append(mu)
                    mu_err_dict["mu_v" + str(i)].append(mu_err)

                    sigma_dict["sig_v" + str(i)].append(sigma)
                    sigma_err_dict["sig_v" + str(i)].append(sigma_err)


                except:
                    # print ("failed to do the gaussian fit")
                    mu_dict["mu_v" + str(i)].append(np.nan)
                    mu_err_dict["mu_v" + str(i)].append(np.nan)

                    sigma_dict["sig_v" + str(i)].append(np.nan)
                    sigma_err_dict["sig_v" + str(i)].append(np.nan)
                    pass

            color = iter(cm.plasma(np.linspace(1, 0, 8)))
            for key_mu in ["mu_h3"]:
                c = next(color)
                ax[1, 1].errorbar(converter_dict_animation[converter_name_list[0]], np.array(mu_dict[key_mu])/1000,
                                    # yerr=np.array(sigma_err_dict[key_sigma])/1000,
                                    yerr = None,
                                    xerr=None,
                                    fmt = "o",
                                    linestyle='dotted',
                                    color = 'k',
                                    markeredgecolor='b',
                                    ecolor="b",
                                    label="$\mu_{H}$")

            # If we did three shots, try to do a linear fit between them
            try:
                choice_fit_acq = "mu_h3"
                if (iteration_number == 2): # Calculate a new linear fit based on the last three points
                    x = [converter_dict_animation[converter_name_list[0]][-3], converter_dict_animation[converter_name_list[0]][-2], converter_dict_animation[converter_name_list[0]][-1]]
                    y = [np.array(mu_dict[choice_fit_acq][-3])/1000, np.array(mu_dict[choice_fit_acq][-2])/1000, np.array(mu_dict[choice_fit_acq][-1])/1000]
                    res = stats.linregress(x, y)
                    ax[1, 1].plot(x, res.intercept + res.slope * np.array(x), color="m", linewidth=5)
            except:
                pass

            color = iter(cm.plasma(np.linspace(1, 0, 8)))
            for key_mu in ["mu_v3"]:
                c = next(color)
                ax[1, 1].errorbar(converter_dict_animation[converter_name_list[0]], np.array(mu_dict[key_mu])/1000,
                                    # yerr=np.array(sigma_err_dict[key_sigma])/1000,
                                    yerr = None,
                                    xerr=None,
                                    fmt = "o",
                                    linestyle='dotted',
                                    color = 'k',
                                    markeredgecolor='r',
                                    ecolor="r",
                                    label="$\mu_{V}$")
            # If we did three shots, try to do a linear fit between them
            try:
                choice_fit_acq = "mu_v3"
                if (iteration_number == 2): # Calculate a new linear fit based on the last three points
                    x = [converter_dict_animation[converter_name_list[0]][-3], converter_dict_animation[converter_name_list[0]][-2], converter_dict_animation[converter_name_list[0]][-1]]
                    y = [np.array(mu_dict[choice_fit_acq][-3])/1000, np.array(mu_dict[choice_fit_acq][-2])/1000, np.array(mu_dict[choice_fit_acq][-1])/1000]
                    res = stats.linregress(x, y)
                    ax_V_sigma.plot(x, res.intercept + res.slope * np.array(x), color="cyan", linewidth=5)
            except:
                pass

            ax[1, 1].set_title(f"Beam centroid {btv_name_list[0]}")
            ax[1, 1].set_xlabel(f"Current {converter_name_list[0]} [A]")
            ax[1, 1].set_ylabel("H beam centroid $\mu$ [m]", color="b")
            ax[1, 1].tick_params(axis='y', labelcolor="b")
            ax[1, 1].set_ylim(-0.05, 0.05)
            ax_V_sigma.set_ylabel("V beam centroid $\mu$ [m]", color="r")
            ax_V_sigma.tick_params(axis='y', labelcolor="r")
            ax_V_sigma.set_ylim(-0.05, 0.05)


        # Append btv image to the new shot
        global btv_data_image
        global number_of_acqu
        btv_data_image = japc.getParam(btv_name_list[0] + "/Image", timingSelectorOverride=East_user)
        number_of_acqu = len(btv_data_image["imageSet"])

        btv_image_dict[btv_name + "/Image"].append(btv_data_image)
        acq_number = 3 # This is the image number you want to display
        if (iteration_number == 2):
            for shot_number in [-3,-2,-1]: # This will take the three last shots
                pixel_y = len(btv_data_image["imagePositionSet2"][0])
                try:
                    image = btv_image_dict[btv_name + "/Image"][shot_number]["imageSet"][acq_number]
                    reshaped_image = image.reshape(pixel_y, -1)
                    ax[1, 0].imshow(reshaped_image, interpolation='nearest', cmap="jet", alpha=1 / 3.33)
                except:
                    pass



        ax[1, 0].set_title(f"Acq")
        ax[1, 0].set_xlabel("x [?]")
        ax[1, 0].set_ylabel("Y [?]")
        ax[1, 0].set_title(f"Average of last three shots on acq{acq_number}")

        # Append all new currents for the new shot
        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict[converter_name].append(current)

        # JSON data dump
        with open(root_folder + "/" + folder_name + "/" + folder_name + ".json", 'w') as outfile:
            outfile.write(json.dumps((timestamp_list, btv_dict, btv_image_dict, converter_dict, steerer_dict), cls=myJSONEncoder))

        # Pickle data dump
        pickle.dump((timestamp_list, btv_dict, btv_image_dict, converter_dict, steerer_dict),
                    open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))

        # Increment the current to scan
        if iteration_number >= (len(current_to_scan) - 1):
            print(f"Resetting array")
            iteration_number = -1 # We restart
        # japc.stopSubscriptions()
        # japc.clearSubscriptions()
        # sys.exit("Scan completed")
        iteration_number += 1

        # Update the plot
        fig.canvas.draw()

        print(f"Setting {quadrupole_to_scan} k to: {round(current_to_scan[iteration_number], 3)}")
        japc.setParam(quadrupole_to_scan, current_to_scan[iteration_number])

        # Save figure
        fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white', transparent=False,
                    bbox_inches='tight');
    else:
        print ("Skipping shot due to low intensity on BCT")
# Everytime this value changes we run the callback function
japc.subscribeParam("F61.BTV012/Acquisition", myCallback, getHeader=True)
japc.startSubscriptions()
fig.show()






# Create the window for the animation of the last BTV shot
fig2, ax2 = plt.subplots(tight_layout=True)
fig2.canvas.manager.set_window_title('Last Shot')
fig2.show()

btv_data_image = japc.getParam(btv_name_list[0] + "/Image")
pixel_y = len(btv_data_image["imagePositionSet2"][0])
image = btv_data_image["imageSet"][1]
reshaped_image = image.reshape(pixel_y, -1)
im = ax2.imshow(reshaped_image, animated=True, cmap="jet")

acq_number = 0
def animate_func(*args):

    global acq_number
    acqTime = japc.getParam(btv_name_list[0] + "/Acquisition#acqTime", timingSelectorOverride=East_user)
    fig2.canvas.manager.set_window_title(f'{btv_name_list[0]} {acqTime}')
    pixel_y = len(btv_data_image["imagePositionSet2"][0])
    try:  # Handling exception if we don't have 7 shots
        image = btv_data_image["imageSet"][acq_number]
        reshaped_image = image.reshape(pixel_y, -1)
        im.set_array(reshaped_image)
    except:
        image = btv_data_image["imageSet"][0]  # If there are not enough frame, just send first frame
        reshaped_image = image.reshape(pixel_y, -1)
    # ax2.set_title(f"Acq: {i}")
    acq_number += 1
    if acq_number == 6:
        acq_number = 0

    return im,

anim = animation.FuncAnimation(
    fig2,
    animate_func,
    blit = True,
    interval=100) # in ms
