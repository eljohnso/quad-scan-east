import pyjapc
import numpy as np
import functions as fs
import pickle

japc = pyjapc.PyJapc(noSet=False)
japc.setSelector("CPS.USER.MD6")
japc.rbacLogin()


bfield_array = np.array([2621, 2675, 2730, 2784, 2891, 2997, 3309])
repetition_number = 10
bfield_array = np.concatenate([([i]*repetition_number) for i in bfield_array], axis=0)

i=0
def myCallback(parameterName, newValue):

    if newValue > 1e9:

        global i

        bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")
        # print(f'Actual B-field = {bfield[0]} [G]')

        new_bfield = bfield_array[i]
        bfield[0] = new_bfield
        print(f'New B-field = {bfield[0]} [G]')
        japc.setParam("PR.MPC/REF.PPPL#REF4", bfield, checkDims=False)

        i+=1
    else:
        print('Skipped on low XSEC')

japc.subscribeParam("T08.XSEC070-I/IntensityMeasurement#intensity", myCallback)
japc.startSubscriptions()