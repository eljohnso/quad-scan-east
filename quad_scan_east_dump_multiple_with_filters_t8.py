import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.dates as mdates
from matplotlib import cm
import datetime
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
import skimage
from skimage import filters
import time
import itertools

time_at_script_start = datetime.datetime.now()


print(f'''
Quadrupole scan with filters
{time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}                                                                                                                                                                                     
''')

# Start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=True)
# japc.rbacLogin()
user = "CPS.USER.EAST4"
japc.setSelector(user)

# BCT in the PSB to skip low intenstiy shots
bct = "EE.BCT10/Acquisition#totalIntensitySingle"
bct_threshold = 1
PSB_user = "LEI.USER.NOMINAL"

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("quad_scan_east_slow_ions_high_energy_2023_data",
                                                        "quad_scan_east_slow_ions_high_energy_2023")

# Data we want to save
timestamp_list = []
intensity_list = []

btv_name_list = ["F61D.BTV010"]
btv_dict = {btv_name + "/Acquisition": [] for btv_name in btv_name_list}
btv_image_dict = {btv_name + "/Image": [] for btv_name in btv_name_list}

filter_wheel = {"no_filter": 'ZERO',
                "filter_1": 'FIRST',
                "filter_2": 'SECOND',
                "filter_3": 'THIRD'}
filter_selection = "no_filter"
print(f"Selected filter: {filter_selection}\n")

converter_name_list = ["F61.QFN01/MEAS.PULSE#VALUE", # Quadrupoles
                       "F61.QDN02/MEAS.PULSE#VALUE",
                       "F61.QFN03/MEAS.PULSE#VALUE",
                       "F61.QDN04/MEAS.PULSE#VALUE",
                       "T8.QFN05/MEAS.PULSE#VALUE",
                       "T8.QDN06/MEAS.PULSE#VALUE",
                       "T8.QDN07/MEAS.PULSE#VALUE",
                       "T8.QFN08/MEAS.PULSE#VALUE"]
converter_dict = {converter_name: [] for converter_name in converter_name_list}

logical_k_list = ["F61.QFN01/MEAS.PULSE#VALUE", # Quadrupoles
                   "F61.QDN02/MEAS.PULSE#VALUE",
                   "F61.QFN03/MEAS.PULSE#VALUE",
                   "F61.QDN04/MEAS.PULSE#VALUE",
                   "T8.QFN05/MEAS.PULSE#VALUE",
                   "T8.QDN06/MEAS.PULSE#VALUE",
                   "T8.QDN07/MEAS.PULSE#VALUE",
                   "T8.QFN08/MEAS.PULSE#VALUE"]
logical_k_dict = {logical_k: [] for logical_k in logical_k_list}

sigma_dict = {"sig_h0": [], "sig_h1": [], "sig_h2": [], "sig_h3": [], "sig_h4": [], "sig_h5": [], "sig_h6": [],
              "sig_v0": [], "sig_v1": [], "sig_v2": [], "sig_v3": [], "sig_v4": [], "sig_v5": [], "sig_v6": []}
sigma_err_dict = {"sig_h0": [], "sig_h1": [], "sig_h2": [], "sig_h3": [], "sig_h4": [], "sig_h5": [], "sig_h6": [],
                  "sig_v0": [], "sig_v1": [], "sig_v2": [], "sig_v3": [], "sig_v4": [], "sig_v5": [], "sig_v6": []}


# Initial BTV parameters
print(80 * "_")
print("Initial BTV settings:\n")

for btv_name in btv_name_list:
    print(btv_name)
    japc = pyjapc.PyJapc(noSet=True)
    japc.setSelector(user)
    print("Image Selection: True,True,True,True,True,True,True")
    japc.setParam(btv_name + "/MultiplexedSetting#reqUserImageSelection", [True, True, True, True, True, True, True])

    japc.setSelector("")  # Parameters that requires no selector
    print("SCREEN: In")
    japc.setParam(btv_name + "/Setting#screenSelect", 'FIRST')  # Screen Out = "ZERO", In = "FIRST"

    print("Camera Switch: ON")
    japc.setParam(btv_name + "/Setting#cameraSwitch", 'CCD_ON_RAD_STDBY')

    print("Lamp Switch: OFF")
    japc.setParam(btv_name + "/Setting#lampSwitch", 'OFF')

    print(f"Filter: {filter_selection}")
    japc.setParam(btv_name + "/Setting#filterSelect", filter_wheel[filter_selection])
    print(" ")

print("Waiting for filter wheel to move")
time.sleep(5)
print("Done")

japc.setSelector(user)
print(80 * "_" + "\n")

print("Starting SCAN\n")
iteration_number = 0

quadrupole_to_scan = "F61.QFN01/REF.PULSE.AMPLITUDE#VALUE"

qfn01_k = japc.getParam('logical.F61.QFN01/K_FUNC_LIST#value')
qdn02_k = japc.getParam('logical.F61.QDN02/K_FUNC_LIST#value')
qfn03_k = japc.getParam('logical.F61.QFN03/K_FUNC_LIST#value')
qdn04_k = japc.getParam('logical.F61.QDN04/K_FUNC_LIST#value')
qfn05_k = japc.getParam('logical.T8.QFN05/K_FUNC_LIST#value')
qdn06_k = japc.getParam('logical.T8.QDN06/K_FUNC_LIST#value')
qdn07_k = japc.getParam('logical.T8.QDN07/K_FUNC_LIST#value')
qfn08_k = japc.getParam('logical.T8.QFN08/K_FUNC_LIST#value')

# Define the K for the magnets
number_of_k_to_scan = 20
repetition_number = 3
length_function_ms = 540 # ms

current1_init = 0.479688
current2_init = -0.114835
current3_init = 0.134012
current4_init = -0.059494
current5_init = 0.129066
current6_init = -0.128044
current7_init = -0.040496
current8_init = 0.045473

# Here we can set which magnet we want to scan
current1 = np.linspace(current1_init, current1_init, number_of_k_to_scan)
current2 = np.linspace(current2_init, current2_init, number_of_k_to_scan)
current3 = np.linspace(current3_init, current3_init, number_of_k_to_scan)
current4 = np.linspace(current4_init, current4_init, number_of_k_to_scan)
current5 = np.linspace(current5_init, current5_init, number_of_k_to_scan)
current6 = np.linspace(current6_init, current6_init, number_of_k_to_scan)
current7 = np.linspace(current7_init, current7_init, number_of_k_to_scan)
current8 = np.linspace(current8_init, current8_init, number_of_k_to_scan)
current1 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in current1))
current2 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in current2))
current3 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in current3))
current4 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in current4))
current5 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in current5))
current6 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in current6))
current7 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in current7))
current8 = list(itertools.chain.from_iterable(itertools.repeat(x, repetition_number) for x in current8))

print('Initial settings:')
print(f"logical.F61.QFN01/K_FUNC_LIST#value to: {round(current1[0], 3)}")
print(f"logical.F61.QDN02/K_FUNC_LIST#value to: {round(current2[0], 3)}")
print(f"logical.F61.QFN03/K_FUNC_LIST#value to: {round(current3[0], 3)}")
print(f"logical.F61.QDN04/K_FUNC_LIST#value to: {round(current4[0], 3)}")
print(f"logical.T8.QFN05/K_FUNC_LIST#value to: {round(current5[0], 3)}")
print(f"logical.T8.QDN06/K_FUNC_LIST#value to: {round(current6[0], 3)}")
print(f"logical.T8.QDN07/K_FUNC_LIST#value to: {round(current7[0], 3)}")
print(f"logical.T8.QFN08/K_FUNC_LIST#value to: {round(current8[0], 3)}")
# japc.rbacLogin()
qfn01_k[0] = [[0, length_function_ms], [current1[iteration_number], current1[iteration_number]]]
qdn02_k[0] = [[0, length_function_ms], [current2[iteration_number], current2[iteration_number]]]
qfn03_k[0] = [[0, length_function_ms], [current3[iteration_number], current3[iteration_number]]]
qdn04_k[0] = [[0, length_function_ms], [current4[iteration_number], current3[iteration_number]]]
qfn05_k[0] = [[0, length_function_ms], [current5[iteration_number], current3[iteration_number]]]
qdn06_k[0] = [[0, length_function_ms], [current6[iteration_number], current3[iteration_number]]]
qdn07_k[0] = [[0, length_function_ms], [current7[iteration_number], current3[iteration_number]]]
qfn08_k[0] = [[0, length_function_ms], [current8[iteration_number], current3[iteration_number]]]
japc.setParam('logical.F61.QFN01/K_FUNC_LIST#value', qfn01_k)
japc.setParam('logical.F61.QDN02/K_FUNC_LIST#value', qdn02_k)
japc.setParam('logical.F61.QFN03/K_FUNC_LIST#value', qfn03_k)
japc.setParam('logical.F61.QDN04/K_FUNC_LIST#value', qdn04_k)
japc.setParam('logical.T8.QFN05/K_FUNC_LIST#value', qfn05_k)
japc.setParam('logical.T8.QDN06/K_FUNC_LIST#value', qdn06_k)
japc.setParam('logical.T8.QDN07/K_FUNC_LIST#value', qdn07_k)
japc.setParam('logical.T8.QFN08/K_FUNC_LIST#value', qfn08_k)

print("Waiting for filter wheel to move")
time.sleep(5)
print("Done")

x_axis = []

fig, ax = plt.subplots(2, 2, figsize=(10, 8), tight_layout=True)

ax_V_sigma = ax[1, 1].twinx()
fig.canvas.manager.set_window_title(
    f'Quadrupole Scan started at: {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
fig.suptitle(f'Quadrupole scan East Dump {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
converter_dict_animation = {converter_name: [] for converter_name in converter_name_list}

# Initial image on the second window
btv_data_image = japc.getParam(btv_name_list[0] + "/Image", timingSelectorOverride=user)
number_of_acqu = len(btv_data_image["imageSet"])


##################
# Callback function, gets called everytime the value we're subscribed to gets updated
##################
def myCallback(parameterName, newValue, header):
    # Check if BCT in the PSB is above threshold
    bct_intensity = japc.getParam(bct, timingSelectorOverride=PSB_user)
    if bct_intensity > bct_threshold:
        ax[0, 0].clear()
        ax[1, 0].clear()
        time = datetime.datetime.now()
        fig.suptitle(time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss"))
        print(f"New Shot for {parameterName} at {time.hour}h{time.minute}:{time.second}")
        global iteration_number
        print(f"Iteration number: {iteration_number}/{number_of_k_to_scan * repetition_number}")
        x_axis.append(iteration_number)
        # Append timestamp
        timestamp_list.append(time)

        # Append intensity
        intensity = japc.getParam("PR.BCT/HotspotIntensity#dcBefEje2", timingSelectorOverride=user)
        intensity_list.append(intensity)

        # Append all new currents for the new shot
        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict[converter_name].append(current)

        # Append all new logical k for the new shot
        for logical_k_name in logical_k_list:
            logical_k = japc.getParam(logical_k_name)
            logical_k_dict[logical_k_name].append(logical_k)


        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict_animation[converter_name].append(current)
            ax[0, 0].plot(timestamp_list, converter_dict_animation[converter_name], marker="o", label=converter_name)
            ax[0, 0].set_xlabel("Local Time")
            ax[0, 0].set_ylabel("Current [A]")
            ax[0, 0].set_title("Converters")
            ax[0, 0].legend()
            ax[0, 0].fmt_xdata = mdates.DateFormatter('%Y-%m-%d')  # Prettier date time
            ax[0, 0].tick_params('x', labelrotation=45)

        # Append all new btv data (projections, etc) for the new shot
        for btv_name in btv_name_list:
            btv_data = japc.getParam(btv_name + "/Acquisition")
            btv_dict[btv_name + "/Acquisition"].append(btv_data)

            try:
                H_mu_list = []
                H_mu_err_list = []
                V_mu_list = []
                V_mu_err_list = []
                H_sigma_list = []
                H_sigma_err_list = []
                V_sigma_list = []
                V_sigma_err_list = []

                colorB = iter(cm.Blues(np.linspace(1, 0, 8)))
                colorR = iter(cm.Reds(np.linspace(1, 0, 8)))
                for i in range(len(btv_dict[btv_name + "/Acquisition"][-1]["positionSet1"])):
                    cB = next(colorB)
                    cR = next(colorR)
                    # Horizontal Gaussian Fit
                    Hx = btv_dict[btv_name + "/Acquisition"][-1]["projPositionSet1"][i]
                    Hy = btv_dict[btv_name + "/Acquisition"][-1]["projDataSet1"][i]

                    try:
                        popt, pcov = fs.do_gaussian_fit(Hx, Hy)
                        H_mu_list.append(popt[2])
                        H_mu_err_list.append(pcov[2, 2] ** 0.5)
                        sigmaH = abs(popt[3])
                        H_sigma_list.append(sigmaH)
                        sigma_err = pcov[3, 3] ** 0.5
                        H_sigma_err_list.append(sigma_err)

                        sigma_dict["sig_h" + str(i)].append(sigmaH)
                        sigma_err_dict["sig_h" + str(i)].append(sigma_err)

                    except:
                        # print ("failed to do the gaussian fit")
                        sigma_dict["sig_h" + str(i)].append(np.nan)
                        sigma_err_dict["sig_h" + str(i)].append(np.nan)
                        pass
                    # Vertical Gaussian Fit
                    Vx = btv_dict[btv_name + "/Acquisition"][-1]["projPositionSet2"][i]
                    Vy = btv_dict[btv_name + "/Acquisition"][-1]["projDataSet2"][i]

                    try:
                        popt, pcov = fs.do_gaussian_fit(Vx, Vy)
                        V_mu_list.append(popt[2])
                        V_mu_err_list.append(pcov[2, 2] ** 0.5)
                        sigmaV = abs(popt[3])
                        V_sigma_list.append(sigmaV)
                        sigma_err = pcov[3, 3] ** 0.5
                        V_sigma_err_list.append(sigma_err)

                        sigma_dict["sig_v" + str(i)].append(sigmaV)
                        sigma_err_dict["sig_v" + str(i)].append(sigma_err)


                    except:
                        # print ("failed to do the gaussian fit")
                        sigma_dict["sig_v" + str(i)].append(np.nan)
                        sigma_err_dict["sig_v" + str(i)].append(np.nan)
                        pass

                    try:
                        if i==1:
                            s=100
                            alpha=0.7
                        else:
                            s=25
                            alpha=0.5
                        ax[1, 1].scatter(logical_k_dict['logical.F61.QFN03/K_FUNC_LIST#value'][-1][0][1][0], sigmaH, s=s, alpha=alpha, color=cB)
                        ax_V_sigma.scatter(logical_k_dict['logical.F61.QFN03/K_FUNC_LIST#value'][-1][0][1][0], sigmaV, s=s, alpha=alpha, color=cR)
                    except:
                        # print('failed plot')
                        pass

                # color = iter(cm.plasma(np.linspace(1, 0, 8)))
                # try:
                #     for key_sigma in ["sig_v3"]:
                #         c = next(color)
                #         ax_V_sigma.errorbar(x_axis, np.array(sigma_dict[key_sigma]) / 1000,
                #                             yerr=np.array(sigma_err_dict[key_sigma]) / 1000,
                #                             xerr=None,
                #                             fmt="o",
                #                             linestyle='dotted',
                #                             color='r',
                #                             markeredgecolor='r',
                #                             ecolor="r",
                #                             label="$\sigma_{V}$")
                # except:
                #     pass

                ax[1, 1].set_title(f"Beam size")
                ax[1, 1].set_xlabel("QFN03")
                ax[1, 1].set_ylabel("H beam size 1$\sigma$ [mm]", color="b")
                ax[1, 1].tick_params(axis='y', labelcolor="b")
                ax_V_sigma.set_ylabel("V beam size 1$\sigma$ [mm]", color="r")
                ax_V_sigma.tick_params(axis='y', labelcolor="r")
            except:
                print('failed to do the plots')

        # Append btv image to the new shot
        global btv_data_image
        global number_of_acqu
        btv_data_image = japc.getParam(btv_name_list[0] + "/Image", timingSelectorOverride=user)
        number_of_acqu = len(btv_data_image["imageSet"])

        btv_image_dict[btv_name + "/Image"].append(btv_data_image)
        for acq_number in range(number_of_acqu):
            pixel_y = len(btv_data_image["imagePositionSet2"][0])
            try:
                image = btv_data_image["imageSet"][acq_number]
                reshaped_image = image.reshape(pixel_y, -1)
                reshaped_image_norm = reshaped_image / 4095  # Normalize
                med_filter_image = filters.median(reshaped_image_norm, np.ones((3, 3)))
                unique, counts = np.unique(med_filter_image, return_counts=True)
                my_dict = dict(zip(unique, counts))
                # Saturation Alarm
                try:
                    saturation_pixels = my_dict[1]
                    if saturation_pixels >= 50:
                        print("SATURATION HIGH!")
                except:
                    pass


            except:
                image = btv_data_image["imageSet"][0]  # If there are not enough frame, just send first frame
                reshaped_image = image.reshape(pixel_y, -1)
            ax[1, 0].imshow(reshaped_image, interpolation='nearest', cmap="jet", alpha=1 / number_of_acqu)
        ax[1, 0].set_title(f"Acq")
        ax[1, 0].set_xlabel("x [?]")
        ax[1, 0].set_ylabel("Y [?]")
        ax[1, 0].set_title(f"Average of {number_of_acqu} acqu.")



        # Update the time of last shot
        global time_last_shot
        time_last_shot = japc.getParam(btv_name_list[0] + "/Acquisition#acqTime", timingSelectorOverride=user)

        try:
            # JSON data dump
            with open(root_folder + "/" + folder_name + "/" + folder_name + ".json", 'w') as outfile:
                outfile.write(json.dumps((timestamp_list, btv_dict, btv_image_dict, converter_dict, logical_k_dict,
                                          intensity_list), cls=myJSONEncoder))

            # Pickle data dump
            pickle.dump((timestamp_list, btv_dict, btv_image_dict, converter_dict, logical_k_dict, intensity_list),
                        open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
        except:
            print('Error on saving data json pickle')

        # Increment the current to scan
        if iteration_number >= (number_of_k_to_scan * repetition_number - 1):
            print(f"SCAN COMPLETED")
            iteration_number = (len(current1) - 2)
        # japc.stopSubscriptions()
        # japc.clearSubscriptions()
        # sys.exit("Scan completed")
        iteration_number += 1

        # Update the plot
        fig.canvas.draw()

        # Change the current in the QUAD
        print(f"logical.F61.QFN01/K_FUNC_LIST#value to: {round(current1[iteration_number], 3)}")
        print(f"logical.F61.QDN02/K_FUNC_LIST#value to: {round(current2[iteration_number], 3)}")
        print(f"logical.F61.QFN03/K_FUNC_LIST#value to: {round(current3[iteration_number], 3)}")
        # japc.rbacLogin()
        qfn01_k[0] = [[0, 490], [current1[iteration_number], current1[iteration_number]]]
        qdn02_k[0] = [[0, 490], [current2[iteration_number], current2[iteration_number]]]
        qfn03_k[0] = [[0, 490], [current3[iteration_number], current3[iteration_number]]]
        japc.setParam('logical.F61.QFN01/K_FUNC_LIST#value', qfn01_k)
        japc.setParam('logical.F61.QDN02/K_FUNC_LIST#value', qdn02_k)
        japc.setParam('logical.F61.QFN03/K_FUNC_LIST#value', qfn03_k)

        try:
            # Save figure
            fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                        transparent=False,
                        bbox_inches='tight');
        except:
            print('error on saving image')
    else:
        print("Skipping shot due to low intensity on BCT")


# Everytime this value changes we run the callback function
print("subscribing")
japc.subscribeParam("F61D.BTV010/Acquisition", myCallback, getHeader=True)
japc.startSubscriptions()
fig.show()

# Create the window for the animation of the last BTV shot
fig2, ax2 = plt.subplots(tight_layout=True)
time_last_shot = japc.getParam(btv_name_list[0] + "/Acquisition#acqTime", timingSelectorOverride=user)
fig2.canvas.manager.set_window_title(f'Last Shot {time_last_shot}')
fig2.show()

btv_data_image = japc.getParam(btv_name_list[0] + "/Image")
pixel_y = len(btv_data_image["imagePositionSet2"][0])
image = btv_data_image["imageSet"][1]
reshaped_image = image.reshape(pixel_y, -1)
im = ax2.imshow(reshaped_image, animated=True, cmap="jet")

acq_number = 0


def animate_func(*args):
    fig2.canvas.manager.set_window_title(f'Last Shot {time_last_shot}')

    global acq_number
    pixel_y = len(btv_data_image["imagePositionSet2"][0])
    try:  # Handling exception if we don't have 7 shots
        image = btv_data_image["imageSet"][acq_number]
        reshaped_image = image.reshape(pixel_y, -1)
        im.set_array(reshaped_image)
    except:
        image = btv_data_image["imageSet"][0]  # If there are not enough frame, just send first frame
        reshaped_image = image.reshape(pixel_y, -1)
        im.set_array(reshaped_image)
    # ax2.set_title(f"Acq: {i}")
    acq_number += 1
    if acq_number == 6:
        acq_number = 0

    return im,


anim = animation.FuncAnimation(
    fig2,
    animate_func,
    blit=True,
    interval=100)  # in ms
