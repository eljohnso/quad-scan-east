import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import functions as fs
import pickle

time_at_script_start = datetime.datetime.now()

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("BCT_monitor",
                                                        "BCT_monitor")

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

fig, ax = plt.subplots(tight_layout=True)
ax2 = ax.twinx()
fig.canvas.manager.set_window_title(f'BCT monitor started at: {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')
fig.suptitle(f'BCT monitor {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')

time_list = [time_at_script_start.strftime("%Hh%Mm%Ss")]

# Data we want to save
timestamp_list = []
bct_name_list = ["PR.BCT/HotspotIntensity#dcBefEje2",
                       "F61.BCT01/Acquisition#totalIntensityAutoCh1",]
bct_dict = {bct: [] for bct in bct_name_list}

def myCallback(parameterName, newValue):

    global time_list

    ax.clear()
    ax2.clear()
    time = datetime.datetime.now()

    time_list.append(time.strftime("%Hh%Mm%Ss"))
    fig.suptitle(time.strftime("BCT \n Most recent acquisition: %Hh%Mm%Ss"))

    # Append all new data for the new shot
    timestamp_list.append(time)
    for bct in bct_name_list:
        data = japc.getParam(bct)
        bct_dict[bct].append(data)

    ax.plot(bct_dict[bct_name_list[0]], marker="o", color="b", alpha=1, label=bct_name_list[0])
    ax2.plot(bct_dict[bct_name_list[1]], marker="o", color="r", alpha=1, label=bct_name_list[1])


    ax.legend(loc="upper left")
    ax2.legend(loc="lower left")

    ax.set_ylim(0,70)
    ax2.set_ylim(0,5)


    fig.canvas.draw() # Update the plot

    try:
        # Pickle data dump
        pickle.dump((timestamp_list, bct_dict), open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
    except:
        print('Error on saving data json pickle')
    try:
        # Save figure
        fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                    transparent=False,
                    bbox_inches='tight');
    except:
        print('error on saving image')

japc.subscribeParam("PR.BCT/HotspotIntensity#dcBefEje2", myCallback)
japc.startSubscriptions()
fig.show()
