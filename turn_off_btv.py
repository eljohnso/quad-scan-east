import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import matplotlib.dates as mdates
import datetime
import os
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
import sys
import time

print(f"Turn OFF BTVs")

btv_name_list = ["F61.BTV012",
		"F62.BTV002",
		"T8.BTV020",
		"T8.BTV035",
		"T8.BTV096"]

# Initial BTV parameters
print(80*"_")
print("Initial BTV settings:\n")


for btv_name in btv_name_list:
	print(btv_name)
	japc = pyjapc.PyJapc(noSet=False)

	japc.setSelector("") #Parameters that requires no selector
	print("Turning OFF Camera Switch")
	japc.setParam(btv_name+"/Setting#screenSelect", 'ZERO')

	print("Retracting Screen")
	japc.setParam(btv_name+"/Setting#cameraSwitch", 'OFF')

	print("Turning OFF Light")
	japc.setParam(btv_name+"/Setting#lampSwitch", 'OFF')
	print(" ")
print(80*"_"+"\n")

print ("Done")
sys.exit()






