import pyjapc

japc = pyjapc.PyJapc(noSet=False)


chirpStartFreqH = 0.32
chirpStopFreqH = 0.333

###################################
### CHIMERA RFKO GAIN SETTING
###
### Please change USER and GAIN
### 1 GeV = CPS.USER.EAST4
### 750 MeV = CPS.USER.EAST3
### 650 MeV = CPS.USER.MD5
###
### Everytime you change cycle, set the gain again
####################################



japc.setSelector("CPS.USER.EAST4")
gain = 0.2 # 0.09 for low intensity, 0.15 for high intensity




japc.setParam("PR.BQL72/Setting#chirpStartFreqH", chirpStartFreqH)
japc.setParam("PR.BQL72/Setting#chirpStopFreqH", chirpStopFreqH)
japc.setParam("PR.BQL72/Setting#exAmplitudeH", gain)

import sys
sys.exit()