import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import cm
import pyjapc
import functions as f
import datetime
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
import time
from skimage import filters

# Data we want to save
timestamp_list = []
intensity_list = []

btv_name_list = ['F61.BTV012', 'F61D.BTV010']
btv_selection = input("Choose BTV:\n Type 0 for F61.BTV012, \n Type 1 for F61D.BTV010\nInput: ")
# btv_selection = 1
btv_name_list = [btv_name_list[int(btv_selection)]]
print(f"Selected BTV: {btv_name_list[0]}\n")

btv_dict = {btv_name: [] for btv_name in btv_name_list}
btv_dict_acquisition = {btv_name: [] for btv_name in btv_name_list}

filter_wheel = {"no_filter": 'ZERO',
                "filter_1": 'FIRST',
                "filter_2": 'SECOND',
                "filter_3": 'THIRD'}

filter_selection = input("Choose filter: no_filter, filter_1, filter_2, filter_3:\nInput: ")
# filter_selection = "no_filter"
print(f"Selected filter: {filter_selection}\n")

print("")

japc = pyjapc.PyJapc(noSet=True)
user = "CPS.USER.MD2"
japc.setSelector(user)

# Create a folder to save data
print(80 * "_")
root_folder, folder_name, time_title = f.create_folder(f"filter_data_east_dump",
                                                       f"filter_data_east_dump_1_turn_" + str(btv_name_list[0]) + "_" + str(
                                                           filter_selection))

# Initial BTV parameters
print(80 * "_")
print("Initial BTV settings:\n")

for btv_name in btv_name_list:
    print(btv_name)
    japc = pyjapc.PyJapc(noSet=False)
    japc.setSelector(user)
    print("Image Selection: True,True,True,True,True,True,True")
    japc.setParam(btv_name + "/MultiplexedSetting#reqUserImageSelection", [True, True, True, True, True, True, True])

    japc.setSelector("")  # Parameters that requires no selector
    print("SCREEN: In")
    japc.setParam(btv_name + "/Setting#screenSelect", 'FIRST')  # Screen Out = "ZERO", In = "FIRST"

    print("Camera Switch: ON")
    japc.setParam(btv_name + "/Setting#cameraSwitch", 'CCD_ON_RAD_STDBY')

    print("Lamp Switch: OFF")
    japc.setParam(btv_name + "/Setting#lampSwitch", 'OFF')

    print(f"Filter: {filter_selection}")
    japc.setParam(btv_name + "/Setting#filterSelect", filter_wheel[filter_selection])
    print(" ")

print("Waiting for filter wheel to move")
time.sleep(5)
print("Done")

japc.setSelector(user)
print(80 * "_" + "\n")

acq = 0
number_of_measurements = 25
fig, ax = plt.subplots(3, 4, constrained_layout=True, figsize=(16, 10))
fig.canvas.manager.set_window_title(
    f'East Area Dump Filter Statistics: {filter_selection} {acq}/{number_of_measurements}')
fig.show()

H_mu_big_list = []
V_mu_big_list = []
H_sigma_big_list = []
V_sigma_big_list = []


# Update the BTV acquisition every new shot from the first BTV in the list
def myCallback(parameterName, newValue, header):
    global acq

    if (acq < number_of_measurements):

        now = datetime.datetime.now()
        print(f"New value for {parameterName} received at {now.strftime('%H')}h{now.strftime('%M')}:{now.strftime('%S')}")

        # Append timestamp
        timestamp_list.append(now)

        # Append intensity
        intensity = japc.getParam("PR.BCT/HotspotIntensity#dcBefEje2")
        intensity_list.append(intensity)

        # Update the data on the BTV acquisitions
        for btv_name in btv_name_list:
            color = iter(cm.plasma(np.linspace(1, 0, 8)))
            btv_data_image = japc.getParam(btv_name + "/Image")
            btv_dict[btv_name].append(btv_data_image)

            btv_data_acquistion = japc.getParam(btv_name + "/Acquisition")
            btv_dict_acquisition[btv_name].append(btv_data_acquistion)

            try:
                ax[0,0].clear()
                ax[0,1].clear()
                ax[0,2].clear()
                ax[0,3].clear()
                ax[1, 0].clear()
                ax[1, 1].clear()
                ax[1, 2].clear()
                ax[1, 3].clear()
                ax[2, 0].clear()
                pass
            except:
                ax[0,0].clear()
                ax[0,1].clear()
                ax[0,2].clear()
                ax[0,3].clear()
                ax[1, 0].clear()
                ax[1, 1].clear()
                ax[1, 2].clear()
                ax[1, 3].clear()
                ax[2, 0].clear()
                pass

            H_mu_list = []
            H_mu_err_list = []
            V_mu_list = []
            V_mu_err_list = []
            H_sigma_list = []
            H_sigma_err_list = []
            V_sigma_list = []
            V_sigma_err_list = []
            Hy_list = []
            Vy_list = []

            H_intensity_list = []
            V_intensity_list = []
            H_intensity_calc_list = []
            V_intensity_calc_list = []

            acquisition = 0
            for i in range(len(btv_dict_acquisition[btv_name][-1]["positionSet1"])):
                c = next(color)
                background_H = btv_dict_acquisition[btv_name][-1]["projDataSet1"][0]
                Hx = btv_dict_acquisition[btv_name][-1]["projPositionSet1"][i]
                Hy = btv_dict_acquisition[btv_name][-1]["projDataSet1"][i] - background_H
                Hy_list.append(Hy)

                H_intensity_calc_list.append(sum(np.diff(Hx) * np.array(Hy[:-1:])))

                try:
                    popt, pcov = f.do_gaussian_fit(Hx, Hy)
                    ax[0,0].plot(Hx, Hy, color=c, zorder=8 - acquisition)
                    ax[0,0].plot(Hx, f.gaussian_function(Hx, popt[0], popt[1], popt[2], popt[3]), color=c,
                               zorder=8 - acquisition, label=(f"Fit Acq: {i}"))

                    H_mu_list.append(popt[2])
                    H_mu_err_list.append(pcov[2, 2] ** 0.5)
                    H_sigma_list.append(abs(popt[3]))
                    H_sigma_err_list.append(pcov[3, 3] ** 0.5)

                except:
                    H_mu_list.append(np.nan)
                    H_mu_err_list.append(np.nan)
                    H_sigma_list.append(np.nan)
                    H_sigma_err_list.append(np.nan)
                    H_intensity_list.append(np.nan)
                    pass

                ax[0,0].set_title(f"Horizontal Proj. {now.strftime('%H')}h{now.strftime('%M')}:{now.strftime('%S')}")

                background_V = btv_dict_acquisition[btv_name][-1]["projDataSet2"][0]
                Vx = btv_dict_acquisition[btv_name][-1]["projPositionSet2"][i]
                Vy = btv_dict_acquisition[btv_name][-1]["projDataSet2"][i] - background_V
                Vy_list.append(Vy)

                V_intensity_calc_list.append(sum(np.diff(Vx) * np.array(Vy[:-1:])))

                try:
                    popt, pcov = f.do_gaussian_fit(Vx, Vy)
                    ax[0,1].plot(Vx, Vy, color=c)
                    ax[0,1].plot(Vx, f.gaussian_function(Vx, popt[0], popt[1], popt[2], popt[3]), color=c,
                               label=(f"Fit Acq: {i}"))
                    V_mu_list.append(popt[2])
                    V_mu_err_list.append(pcov[2, 2] ** 0.5)
                    V_sigma_list.append(abs(popt[3]))
                    V_sigma_err_list.append(pcov[3, 3] ** 0.5)
                except:
                    V_mu_list.append(np.nan)
                    V_mu_err_list.append(np.nan)
                    V_sigma_list.append(np.nan)
                    V_sigma_err_list.append(np.nan)
                    V_intensity_list.append(np.nan)
                    pass
                acquisition += 1

                ax[0,1].set_title(f"Vertical Proj. {now.strftime('%H')}h{now.strftime('%M')}:{now.strftime('%S')}")

            acquisition_list = [0, 1, 2, 3, 4, 5]
            # Plotting the Histogram of centroid
            H_mu_big_list.append(H_mu_list)
            V_mu_big_list.append(V_mu_list)
            H_sigma_big_list.append(H_sigma_list)
            V_sigma_big_list.append(V_sigma_list)
            try:
                matrix_H_mu = np.array(H_mu_big_list)
                matrix_V_mu = np.array(V_mu_big_list)
                matrix_H_sigma = np.array(H_sigma_big_list)
                matrix_V_sigma = np.array(V_sigma_big_list)
                for i in range(len(acquisition_list)):
                    try:
                        # Centroids mu
                        # H
                        ax[1,0].violinplot(matrix_H_mu[:, i][~np.isnan(matrix_H_mu[:, i])], [acquisition_list[i]],
                                         vert=False, showmeans=True, showextrema=False, showmedians=False)
                        try:
                            ax[1,0].scatter(matrix_H_mu[:, i][~np.isnan(matrix_H_mu[:, i])],
                                          np.ones(len(matrix_H_mu[:, i][~np.isnan(matrix_H_mu[:, i])])) * i, s=2,
                                          color="k", zorder=99)
                        except:
                            pass
                        # V
                        ax[1,1].violinplot(matrix_V_mu[:, i][~np.isnan(matrix_V_mu[:, i])], [acquisition_list[i]],
                                         vert=False, showmeans=True, showextrema=False, showmedians=False)
                        try:
                            ax[1,1].scatter(matrix_V_mu[:, i][~np.isnan(matrix_V_mu[:, i])],
                                          np.ones(len(matrix_V_mu[:, i][~np.isnan(matrix_V_mu[:, i])])) * i, s=2,
                                          color="k", zorder=99)
                        except:
                            pass
                        # Beam size sigma
                        # H
                        ax[1,2].violinplot(matrix_H_sigma[:, i][~np.isnan(matrix_H_sigma[:, i])], [acquisition_list[i]],
                                         vert=False, showmeans=True, showextrema=False, showmedians=False)
                        try:
                            ax[1,2].scatter(matrix_H_sigma[:, i][~np.isnan(matrix_H_sigma[:, i])],
                                          np.ones(len(matrix_H_sigma[:, i][~np.isnan(matrix_H_sigma[:, i])])) * i, s=2,
                                          color="k", zorder=99)
                        except:
                            pass
                        # V
                        ax[1,3].violinplot(matrix_V_sigma[:, i][~np.isnan(matrix_V_sigma[:, i])], [acquisition_list[i]],
                                         vert=False, showmeans=True, showextrema=False, showmedians=False)
                        try:
                            ax[1,3].scatter(matrix_V_sigma[:, i][~np.isnan(matrix_V_sigma[:, i])],
                                          np.ones(len(matrix_V_sigma[:, i][~np.isnan(matrix_V_sigma[:, i])])) * i, s=2,
                                          color="k", zorder=99)
                        except:
                            pass
                    except:
                        pass
                ax[1,0].set_title("Horizontal centroid")
                ax[1,0].set_ylabel("Acqu no.")
                ax[1,0].set_xlabel("$\mu_{H}$")
                ax[1,0].set_xlim(-50, 50)
                ax[1,1].set_title("Vertical centroid")
                ax[1,1].set_ylabel("Acqu no.")
                ax[1,1].set_xlabel("$\mu_{V}$")
                ax[1,1].set_xlim(-50, 50)
                # Beam size
                ax[1,2].set_title("Horizontal beam size")
                ax[1,2].set_ylabel("Acqu no.")
                ax[1,2].set_xlabel("$\sigma_{H}$")
                ax[1,3].set_title("Vertical beam size")
                ax[1,3].set_ylabel("Acqu no.")
                ax[1,3].set_xlabel("$\sigma_{H}$")
            except:
                print("Failed to plot histogram")

            # Plot projection close to centroid to see Saturation
            for i in range(len(acquisition_list)):
                pixel_y = len(btv_dict[btv_name][-1]["imagePositionSet2"][i])
                image = btv_dict[btv_name][-1]["imageSet"][i]
                reshaped_image = image.reshape(pixel_y, -1)
                mylinspace = np.linspace(0, len(reshaped_image.sum(axis=0)) - 1, len(reshaped_image.sum(axis=0)))
                gaussian_filter_image = filters.gaussian(reshaped_image, sigma=2)

                Hx = mylinspace
                Hy = gaussian_filter_image.sum(axis=0)
                ax[0, 2].set_title("Projection at $\mu_{H}$")
                ax[0, 3].set_title("Projection at $\mu_{V}$")
                try:
                    popt, pcov = f.do_gaussian_fit(Hx, Hy)
                    ax[0, 2].plot(np.array(gaussian_filter_image)[:, int(popt[2])], label=f"Saturation detector {i}")
                    popt, pcov = f.do_gaussian_fit(Vx, Vy)
                    ax[0, 3].plot(np.array(gaussian_filter_image)[int(popt[2]), :], label=f"Saturation detector {i}")
                except:
                    pass

            # Plot histogram of intensity
            ax[2,0].hist(intensity_list)
            ax[2,0].set_title("CPS Intensity dcBefEj2")
            ax[2,0].set_xlim(0,100)
            ax[2,0].set_xlabel("# proton $10^{10}$")


            ax2.set_title(f"{btv_name} {now.strftime('%H')}h{now.strftime('%M')}:{now.strftime('%S')}")
            fig2.canvas.manager.set_window_title(
                f'East Area Dump Filter Statistics: {filter_selection} {acq}/{number_of_measurements}')

            try:
                # JSON data dump
                with open(root_folder + "/" + folder_name + "/" + folder_name + ".json", 'w') as outfile:
                    outfile.write(json.dumps((timestamp_list, btv_dict, btv_dict_acquisition, intensity_list), cls=myJSONEncoder))

                # Pickle data dump
                pickle.dump((timestamp_list, btv_dict, btv_dict_acquisition, intensity_list),
                            open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
            except:
                print('Error on saving data json or pickle')

            try:
                # Save figure
                fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                            transparent=False, bbox_inches='tight');
            except:
                print('Error on saving image')

        acq += 1
        fig.canvas.manager.set_window_title(
            f'East Area Dump Filter Statistics: {filter_selection} {acq}/{number_of_measurements}')

    else:
        print("")
        japc.stopSubscriptions()
        for btv_name in btv_name_list:
            print("Restoring BTV settings")
            japc.setSelector("")  # Parameters that requires no selector
            print("SCREEN: Out")
            japc.setParam(btv_name + "/Setting#screenSelect", 'ZERO')  # Screen Out = "ZERO", In = "First"

            print("Camera Switch: OFF")
            japc.setParam(btv_name + "/Setting#cameraSwitch", 'OFF')

            print("Lamp Switch: OFF")
            japc.setParam(btv_name + "/Setting#lampSwitch", 'OFF')

            print(f"Filter: no_filter")
            japc.setParam(btv_name + "/Setting#filterSelect", "ZERO")
            print(" ")

        print("END of measurement")

    fig.canvas.draw_idle()
    fig2.canvas.draw_idle()


japc.subscribeParam(btv_name_list[0] + "/Image", myCallback, getHeader=True)
japc.startSubscriptions()

image_dict = {btv_name: [] for btv_name in btv_name_list}

fig2, ax2 = plt.subplots(figsize=(8,6))
fig2.canvas.manager.set_window_title(
    f'East Area Dump Filter Statistics: {filter_selection} {acq}/{number_of_measurements}')
fig2.show()

# Creation of the initial images object
i = 0
for btv_name in btv_name_list:
    now = datetime.datetime.now()
    btv_data_image = japc.getParam(btv_name + "/Image")
    pixel_y = len(btv_data_image["imagePositionSet2"][0])
    image = btv_data_image["imageSet"][0]
    reshaped_image = image.reshape(pixel_y, -1)
    im = ax2.imshow(reshaped_image, animated=True, cmap="plasma")
    ax2.set_title(f"{btv_name} {now.strftime('%H')}h{now.strftime('%M')}:{now.strftime('%S')}")
    image_dict[btv_name].append(im)
    i += 1

i = 0


def updatefig(*args):
    global i
    list_of_artists = []
    try:
        for btv_name in btv_name_list:
            pixel_y = len(btv_dict[btv_name][-1]["imagePositionSet2"][0])
            background_image = btv_dict[btv_name][-1]["imageSet"][0]
            image = btv_dict[btv_name][-1]["imageSet"][i]
            image = image - background_image
            reshaped_image = image.reshape(pixel_y, -1)
            image_dict[btv_name][-1].set_array(reshaped_image)
            list_of_artists.append(image_dict[btv_name][-1])

    except:  # Missing acquisition we print a cross
        reshaped_image = np.full((264, 330), 0)
        reshaped_image[:, 160:170] = 1000
        reshaped_image[127:137, :] = 1000
        image_dict[btv_name][-1].set_array(reshaped_image)
        list_of_artists.append(image_dict[btv_name][-1])
        pass
    i += 1
    if i == 7:
        i = 0
    return list_of_artists


ani = animation.FuncAnimation(fig2, updatefig, interval=100,  # ms
                              blit=True)
