import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import cm
import pyjapc
import functions as f
import scipy.integrate as integrate
import datetime

btv_name_list = [
		#"PR.BTV57",
		#"F61.BTV012",
		#"F62.BTV002",
		#"T8.BTV020",
		#"T8.BTV035",
		"T8.BTV096",
		#"TMMTV0026"
		"TMMTV0009"
		]

btv_3d = btv_name_list[0]


# btv_name_list = ["F61.BTV012"]

btv_name_list = btv_name_list[0::1]
btv_dict = {btv_name: [] for btv_name in btv_name_list}

btv_dict_acquisition = {btv_name: [] for btv_name in btv_name_list}

japc = pyjapc.PyJapc(noSet=False)
user = "CPS.USER.EAST1"
japc.setSelector(user)

# Initial BTV parameters
print(80*"_")
print("Initial BTV settings:\n")

for btv_name in btv_name_list:
	print(btv_name)
	japc = pyjapc.PyJapc(noSet=False)
	japc.setSelector(user)
	print("Selecting all images to acquire")
	japc.setParam(btv_name+"/MultiplexedSetting#reqUserImageSelection", [True,True,True,True,True,True,True])

	japc.setSelector("") #Parameters that requires no selector
	print("Turning on Camera Switch")
	japc.setParam(btv_name+"/Setting#screenSelect", 'FIRST')

	print("Inserting Screen")
	japc.setParam(btv_name+"/Setting#cameraSwitch", 'CCD_ON_RAD_STDBY')

	print("Turning OFF Light")
	japc.setParam(btv_name+"/Setting#lampSwitch", 'OFF')
	print(" ")

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector(user)
print(80*"_"+"\n")

fig, ax = plt.subplots(6,len(btv_name_list),constrained_layout=True, figsize=(len(btv_name_list)*3,9))
fig.canvas.manager.set_window_title('East Area Sweep Analysis')
fig.show()

fig_zoom, ax_zoom = plt.subplots(1,len(btv_name_list),constrained_layout=True, figsize=(len(btv_name_list)*3,9))
fig_zoom.canvas.manager.set_window_title('East Area Zoom')
fig_zoom.show()

# fig3d = plt.figure()
# fig3d.canvas.manager.set_window_title('3D')
# fig3d.show()

# Update the BTV acquisition every new shot from the first BTV in the list
def myCallback(parameterName, newValue, header):
	now = datetime.datetime.now()
	print(f"New value for {parameterName} {now.hour}h{now.minute}:{now.second}")

	# Update the data on the BTV acquisitions
	acq = 0
	for btv_name in btv_name_list:
		color = iter(cm.plasma(np.linspace(1, 0, 8)))
		btv_data_image = japc.getParam(btv_name+"/Image")
		btv_dict[btv_name].append(btv_data_image)

		btv_data_image = japc.getParam(btv_name + "/Acquisition")
		btv_dict_acquisition[btv_name].append(btv_data_image)

		try:
			#ax[1].clear()
			#ax[2].clear()
			pass
		except:
			#ax[1,acq].clear()
			#ax[2,acq].clear()
			#ax[3,acq].clear()
			#ax[4,acq].clear()
			ax[5, acq].clear()
			pass

		H_mu_list = []
		H_mu_err_list = []
		V_mu_list = []
		V_mu_err_list = []
		H_sigma_list = []
		H_sigma_err_list = []
		V_sigma_list = []
		V_sigma_err_list = []
		Hy_list = []
		Vy_list = []

		H_intensity_list = []
		V_intensity_list = []
		H_intensity_calc_list = []
		V_intensity_calc_list = []

		acquisition = 0
		for i in range(len(btv_dict_acquisition[btv_name][-1]["positionSet1"])):
			c = next(color)
			background_H = btv_dict_acquisition[btv_name][-1]["projDataSet1"][0]
			Hx = btv_dict_acquisition[btv_name][-1]["projPositionSet1"][i]
			Hy = btv_dict_acquisition[btv_name][-1]["projDataSet1"][i] - background_H
			Hy_list.append(Hy)

			H_intensity_calc_list.append( sum(np.diff(Hx)*np.array(Hy[:-1:])) )

			if (btv_name == btv_3d):
				Hx3d = Hx
				Hy3d_list = Hy_list

			try:
				popt, pcov = f.do_gaussian_fit(Hx,Hy)
				ax[1, acq].plot(Hx, Hy, color = c, zorder=8-acquisition)
				ax[1,acq].plot(Hx, f.gaussian_function(Hx,popt[0],popt[1], popt[2], popt[3]), color = c, zorder=8-acquisition, label=(f"Fit Acq: {i}"))
				# ax[1,acq].legend()
				H_mu_list.append(popt[2])
				H_mu_err_list.append(pcov[2, 2] ** 0.5)
				H_sigma_list.append(abs(popt[3]))
				H_sigma_err_list.append(pcov[3, 3] ** 0.5)
				#H_intensity_list.append(integrate.quad(lambda x: f.gaussian_function(x,popt[0],popt[1], popt[2], popt[3]), -np.inf, np.inf)[0])

			except:
				H_mu_list.append(np.nan)
				H_mu_err_list.append(np.nan)
				H_sigma_list.append(np.nan)
				H_sigma_err_list.append(np.nan)
				H_intensity_list.append(np.nan)
				pass

			ax[1,acq].set_title(f"Horizontal {now.hour}h{now.minute}:{now.second}")


			background_V = btv_dict_acquisition[btv_name][-1]["projDataSet2"][0]
			Vx = btv_dict_acquisition[btv_name][-1]["projPositionSet2"][i]
			Vy = btv_dict_acquisition[btv_name][-1]["projDataSet2"][i] - background_V
			Vy_list.append(Vy)

			V_intensity_calc_list.append(sum(np.diff(Vx) * np.array(Vy[:-1:])))

			if (btv_name == btv_3d):
				Vx3d = Vx
				Vy3d_list = Vy_list

			try:
				popt, pcov = f.do_gaussian_fit(Vx,Vy)
				ax[2,acq].plot(Vx, Vy, color=c)
				ax[2,acq].plot(Vx, f.gaussian_function(Vx,popt[0],popt[1], popt[2], popt[3]), color=c, label=(f"Fit Acq: {i}"))
				# ax[2,acq].legend()
				V_mu_list.append(popt[2])
				V_mu_err_list.append(pcov[2, 2] ** 0.5)
				V_sigma_list.append(abs(popt[3]))
				V_sigma_err_list.append(pcov[3, 3] ** 0.5)
				#V_intensity_list.append(integrate.quad(lambda x: f.gaussian_function(x, popt[0], popt[1], popt[2], popt[3]), -100, 100)[0])
			except:
				V_mu_list.append(np.nan)
				V_mu_err_list.append(np.nan)
				V_sigma_list.append(np.nan)
				V_sigma_err_list.append(np.nan)
				V_intensity_list.append(np.nan)
				pass
			acquisition +=1

			ax[2,acq].set_title("Vertical")

		ctime = btv_dict_acquisition[btv_name][-1]["acqTimeInCycle"]
		ax[3, acq].errorbar(H_mu_list, ctime, yerr=None, xerr=H_mu_err_list, color = "r", label = "$\mu_{H}$")
		ax[3, acq].errorbar(V_mu_list, ctime, yerr=None, xerr=V_mu_err_list, color = "b", label="$\mu_{V}$")
		ax[3,acq].set_title("Mean position")
		ax[3,acq].set_xlim(-30,30)
		ax[3,acq].set_xlabel(f"$\mu$ [mm]")
		ax[3,acq].set_ylabel(f"ctime [ms]")
		#ax[3, acq].legend()
		ax[3, acq].grid()

		ax_zoom[acq].errorbar(H_mu_list, ctime, yerr=None, xerr=H_mu_err_list, color = "r", label = "$\mu_{H}$")
		ax_zoom[acq].errorbar(V_mu_list, ctime, yerr=None, xerr=V_mu_err_list, color = "b", label="$\mu_{V}$")
		ax_zoom[acq].set_title("Mean position")
		ax_zoom[acq].set_xlim(-30,30)
		ax_zoom[acq].set_xlabel(f"$\mu$ [mm]")
		ax_zoom[acq].set_ylabel(f"ctime [ms]")
		#ax[3, acq].legend()
		ax_zoom[acq].grid()

		ax[4, acq].errorbar(H_sigma_list, ctime, yerr=None, xerr=H_sigma_err_list, color = "r", label="$\sigma_{H}$")
		ax[4, acq].errorbar(V_sigma_list, ctime, yerr=None, xerr=V_sigma_err_list, color = "b", label="$\sigma_{V}$")
		ax[4,acq].set_title("Beam size")
		ax[4,acq].set_xlim(-20,+20)
		ax[4,acq].set_xlabel(f"$\sigma$ [mm]")
		ax[4,acq].set_ylabel(f"ctime [ms]")
		#ax[4, acq].legend()
		ax[4, acq].grid()

		#ax[5,acq].plot(ctime, H_intensity_list, marker="o", color = "r", linestyle = "--", label="H fit")
		ax[5,acq].plot(ctime, H_intensity_calc_list, marker="o", color="r", label="H calc")
		#ax[5,acq].plot(ctime, V_intensity_list, marker="o", color="b", linestyle = "--", label="V fit")
		ax[5,acq].plot(ctime, V_intensity_calc_list, marker="o", color="b", label="V calc")
		#ax[5,acq].plot(ctime, np.array(H_intensity_list)+np.array(V_intensity_list), marker="o", color="m", linestyle = "--", label="H+V fit")
		ax[5, acq].plot(ctime, np.array(H_intensity_calc_list) + np.array(V_intensity_calc_list), marker="o", color="m", label="H+V calc")
		ax[5,acq].set_title("Intensity")
		ax[5,acq].set_xlabel(f"ctime [ms]")
		ax[5,acq].set_ylabel(f"Intensity [arb.]")
		ax[5,acq].set_ylim(0,3e7)
		#ax[5,acq].legend()
		ax[5,acq].grid()

		acq += 1

	### 3d plot
	# ax3d = fig3d.add_subplot(121, projection='3d')
	# ax3d.clear()
	#
	# x = Hx3d
	# y = ctime
	# X, Y = np.meshgrid(x, y)
	# Z = np.array(Hy3d_list)
	#
	# ax3d.plot_surface(X, Y, Z, rstride=1, cstride=1000)
	#
	# ax3d.set_xlabel("H Position [mm]")
	# ax3d.set_ylabel("ctime [ms]")
	# ax3d.set_zlabel("Ampltitude [arb.]")
	# ax3d.view_init(20, -120)
	#
	# ax3d = fig3d.add_subplot(122, projection='3d')
	#
	# x = Vx3d
	# y = ctime
	# X, Y = np.meshgrid(x, y)
	# Z = np.array(Vy3d_list)
	#
	# ax3d.plot_surface(X, Y, Z, rstride=1, cstride=1000)
	#
	# ax3d.set_xlabel("V Position [mm]")
	# ax3d.set_ylabel("ctime [ms]")
	# ax3d.set_zlabel("Ampltitude [arb.]")
	# ax3d.view_init(20, -120)

	fig.canvas.draw_idle()
	fig_zoom.canvas.draw_idle()
	# ax3d._facecolors2d = ax3d._facecolor
	# fig3d.canvas.draw_idle()


japc.subscribeParam(btv_name_list[0]+"/Image", myCallback, getHeader=True)
japc.startSubscriptions()

image_dict = {btv_name: [] for btv_name in btv_name_list}

# Creation of the initial images object
i = 0
for btv_name in btv_name_list:
	btv_data_image = japc.getParam(btv_name+"/Image")
	pixel_y = len(btv_data_image["imagePositionSet2"][0])
	image = btv_data_image["imageSet"][0]
	reshaped_image = image.reshape(pixel_y,-1)
	try:
		im = ax[0,i].imshow(reshaped_image, animated=True, cmap="plasma")
		ax[0,i].set_title(f"{btv_name}")
	except: # if you only have one BTV in the list
		im = ax.imshow(reshaped_image, animated=True, cmap="plasma")
		ax.set_title(f"{btv_name}")
	image_dict[btv_name].append(im)
	i+=1

i = 0
def updatefig(*args):
	global i
	list_of_artists = []
	try:
		for btv_name in btv_name_list:
			pixel_y = len(btv_dict[btv_name][-1]["imagePositionSet2"][0])
			background_image = btv_dict[btv_name][-1]["imageSet"][0]
			image = btv_dict[btv_name][-1]["imageSet"][i]
			image = image - background_image
			reshaped_image = image.reshape(pixel_y,-1)
			image_dict[btv_name][-1].set_array(reshaped_image)
			list_of_artists.append(image_dict[btv_name][-1])
			
	except: # Missing acquisition we print a cross
		reshaped_image = np.full((264,330), 0)
		reshaped_image[:,160:170] = 1000
		reshaped_image[127:137,:] = 1000
		image_dict[btv_name][-1].set_array(reshaped_image)
		list_of_artists.append(image_dict[btv_name][-1])
		pass
	i+=1
	if i==7:
		i=0
	return list_of_artists

ani = animation.FuncAnimation(fig, updatefig, interval=100, # ms
 blit=True)
# plt.show()
